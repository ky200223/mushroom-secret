/**
 * Created by youngkim on 2014. 11. 29..
 */

var user = (function () {
    var userId;

    var get = function () {
        return userId;
    };

    var set = function (id) {
        userId = id;
    };

    return {
        get: get,
        set: set
    }
})();

var subscribe = function (e) {
    var sendData = {
        user_id: user.get(),
        artist_id: e.target.parentNode.childNodes[2].innerText
    };

    $.ajax({
        type: "POST",
        async: true,
        url: '/user/subscribe',
        data: sendData,
        success: function () {
            var subscribeBtn = e.target;
            subscribeBtn.setAttribute('id', 'subscribed');
            subscribeBtn.setAttribute('src', '/images/subscribed.png');
            $(subscribeBtn).unbind('click');
            $(subscribeBtn).click(function (e) {
                e.stopPropagation();
                unSubscribe(e);
            });
        }
    });
};

var unSubscribe = function (e) {
    var sendData = {
        user_id: user.get(),
        artist_id: e.target.parentNode.childNodes[2].innerText
    };

    $.ajax({
        type: "POST",
        async: true,
        url: '/user/unSubscribe',
        data: sendData,
        success: function () {
            var unSubscribeBtn = e.target;
            unSubscribeBtn.setAttribute('id', 'subscribe');
            unSubscribeBtn.setAttribute('src', '/images/subscribe.png');
            $(unSubscribeBtn).unbind('click');
            $(unSubscribeBtn).click(function (e) {
                e.stopPropagation();
                subscribe(e);
            });
        }
    });
};

var like = function (e) {
    var sendData = {
        user_id: user.get(),
        artwork_id: e.target.parentNode.parentNode.childNodes[2].innerText
    };

    $.get('http://54.64.254.152/?user_id=' + user.get()
    + '&item_id=' + sendData.artwork_id + '&status=2', function () {
        var likeBtn = e.target;
        likeBtn.setAttribute('id', 'liked');
        likeBtn.setAttribute('src', '/images/browse_liked.png');
        $(likeBtn).unbind('click');
        $(likeBtn).click(function (e) {
            e.stopPropagation();
            unLike(e);
        });
    });
};

var unLike = function (e) {
    var sendData = {
        user_id: user.get(),
        artwork_id: e.target.parentNode.parentNode.childNodes[2].innerText
    };

    $.get('http://54.64.254.152/?user_id=' + user.get()
    + '&item_id=' + sendData.artwork_id + '&status=-1', function () {
        var likedBtn = e.target;
        likedBtn.setAttribute('id', 'like');
        likedBtn.setAttribute('src', '/images/browse_like.png');
        $(likedBtn).unbind('click');
        $(likedBtn).click(function (e) {
            e.stopPropagation();
            like(e);
        });
    });
};

var createArtworkCards = function (dataArray) {
    dataArray.forEach(function (value) {
        createArtworkCard(value);
    });
};

var createArtworkCard = function (data) {
    var artworkFrag = document.createDocumentFragment();

    var artCard = document.createElement('div');
    artCard.classList.add('artCard');
    artCard.onclick = function () {
        window.location.href = '/user/page/' + data.artistId;
    };

    var artworkOverlay = document.createElement('div');
    artworkOverlay.classList.add('artworkOverlay');

    artCard.appendChild(artworkOverlay);

    var artworkInfo = document.createElement('div');
    artworkInfo.classList.add('artworkInfo');

    var introduction = document.createElement('div');
    introduction.classList.add('introduction');
    introduction.innerText = data.title;

    var nameLikeDiv = document.createElement('div');
    nameLikeDiv.classList.add('nameLikeDiv');

    var artistNickname = document.createElement('div');
    artistNickname.classList.add('artistNickname');
    artistNickname.innerText = data.nickname;

    var likeBtn = document.createElement('img');
    if (data.like_flag === 1 || data.like_flag === undefined) {
        likeBtn.setAttribute('src', '/images/browse_liked.png');
        likeBtn.classList.add('liked');
        $(likeBtn).click(function (e) {
            e.stopPropagation();
            unLike(e);
        });
    } else if (data.like_flag === 0) {
        likeBtn.setAttribute('src', '/images/browse_like.png');
        likeBtn.classList.add('like');
        $(likeBtn).click(function (e) {
            e.stopPropagation();
            like(e);
        });
    }

    var artworkId = document.createElement('div');
    artworkId.classList.add('artworkId');
    artworkId.innerText = data.id;

    artworkInfo.appendChild(introduction);

    nameLikeDiv.appendChild(artistNickname);
    nameLikeDiv.appendChild(likeBtn);

    artworkInfo.appendChild(nameLikeDiv);
    artworkInfo.appendChild(artworkId);

    artCard.appendChild(artworkInfo);

    var thumbnail = document.createElement('img');
    var imgSrc = 'http://do086lo4rikup.cloudfront.net/' + data.thumbnail_url;
    thumbnail.setAttribute('src', imgSrc);

    artCard.appendChild(thumbnail);

    artworkFrag.appendChild(artCard);

    document.getElementById('cardList').appendChild(artworkFrag);
};

var createArtistCards = function (dataArray) {
    dataArray.forEach(function (value) {
        createArtistCard(value);
    });
};

var createArtistCard = function (data) {
    var artworkFrag = document.createDocumentFragment();

    var artistCard = document.createElement('div');
    artistCard.classList.add('artistCard');
    artistCard.onclick = function () {
        window.location.href = '/user/page/' + data.id;
    };

    var artistOverlay = document.createElement('div');
    artistOverlay.classList.add('artistOverlay');

    artistCard.appendChild(artistOverlay);

    var artistInfo = document.createElement('div');
    artistInfo.classList.add('artistInfo');

    var subscribeBtn = document.createElement('img');
    subscribeBtn.setAttribute('src', '/images/subscribed.png');
    subscribeBtn.classList.add('subscribed');
    $(subscribeBtn).click(function (e) {
        e.stopPropagation();
        unSubscribe(e);
    });

    var artistNickname = document.createElement('div');
    artistNickname.classList.add('artistNickname');
    artistNickname.innerText = data.nickname;

    var artistId = document.createElement('div');
    artistId.classList.add('artistId');
    artistId.innerText = data.id;

    artistInfo.appendChild(subscribeBtn);
    artistInfo.appendChild(artistNickname);
    artistInfo.appendChild(artistId);

    artistCard.appendChild(artistInfo);

    var thumbnail = document.createElement('img');
    if (data.profile_url === '0' || data.profile_url === 0) {
        var imgSrc = '/user_profile/uploadPrefix/profile.jpg';
    } else {
        var imgSrc = 'http://do086lo4rikup.cloudfront.net/' + data.profile_url;
    }
    thumbnail.setAttribute('src', imgSrc);

    artistCard.appendChild(thumbnail);

    artworkFrag.appendChild(artistCard);

    document.getElementById('cardList').appendChild(artworkFrag);
};

var sendIndex = 0;
var sendMoreQuery = function (callback) {
    var sendUrl = window.location.pathname;

    sendIndex = sendIndex + 18;
    var data = {
        sendIndex: sendIndex
    };

    $.ajax({
        type: "POST",
        async: true,
        url: sendUrl,
        data: data,
        success: function (data) {
            if (data.end) {
                $('#loadMore').css('display', 'none');
                $('#footerSpacer').css('display', 'block');
            }
            if (data.flag === 'artwork') {
                createArtworkCards(data.result);
            } else if (data.flag === 'artist') {
                createArtistCards(data.result);
            }
            callback();
        }
    }).fail(function () {
        sendIndex = sendIndex - 18;
    });
};

$(document).ready(function () {
    $.get('/user/getUserId', function (data) {
    }).done(function (data) {
        user.set(data.id);
    });

    $('#cardList .subscribe').click(function (e) {
        e.stopPropagation();
        subscribe(e);
    });

    $('#cardList .subscribed').click(function (e) {
        e.stopPropagation();
        unSubscribe(e);
    });

    $('#cardList .like').click(function (e) {
        like(e);
        e.stopPropagation();
    });

    $('#cardList .liked').click(function (e) {
        unLike(e);
        e.stopPropagation();
    });

    $('#loadMore').click(function () {
        sendMoreQuery(function() {
            sendMore = true;
        });
    });
});

var sendMore = true;
$(window).scroll(function () {
    if ($(window).scrollTop() + window.innerHeight >= $(document).height() && $('#loadMore').css('display') !== 'none'
        && sendMore) {
        sendMore = false;
        sendMoreQuery(function() {
            sendMore = true;
        });
    }
});