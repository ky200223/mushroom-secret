/**
 * Created by youngkim on 2014. 11. 12..
 */

'use strict';

function getFixedXY(obj) {
    var ret = new Object();
    var rect = obj.getBoundingClientRect();
    ret.left = rect.left;
    ret.top = rect.top;
    ret.width = rect.right - rect.left;
    ret.height = rect.bottom - rect.top;
    return ret;
}
function getAbsoluteXY(obj) {
    var ret = new Object();
    var rect = obj.getBoundingClientRect();
    ret.left = rect.left + (document.documentElement.scrollLeft || document.body.scrollLeft);
    ret.top = rect.top + (document.documentElement.scrollTop || document.body.scrollTop);
    ret.width = rect.right - rect.left;
    ret.height = rect.bottom - rect.top;
    return ret;
}

var logoBtn = document.getElementById('logoNavButton');

logoBtn.addEventListener('mouseover', function () {
    var dropDown = document.getElementById('dropDownContainer');
    var pos = getFixedXY(this);
    dropDown.style.left = pos.left + pos.width / 2;
    dropDown.style.top = pos.top + pos.height;
    dropDown.style.display = 'block';
});

logoBtn.addEventListener('mouseout', function () {
    var dropDown = document.getElementById('dropDownContainer');
    dropDown.style.display = 'none';
});

var dropMenu = document.getElementById("dropDownContainer");

dropMenu.addEventListener('mouseover', function () {
    this.style.display = 'block';
});

dropMenu.addEventListener('mouseout', function () {
    this.style.display = 'none';
});

var prevScrollTop = document.body.scrollTop;
var chkScroll = function () {
    var curScrollTop = document.body.scrollTop;
    if (curScrollTop <= 0) {
        expandBar(1);
        return;
    }
    if (curScrollTop > prevScrollTop) {
        if (curScrollTop > 70)
            expandBar(0);
    }
    else
        expandBar(1);

    prevScrollTop = curScrollTop;
};
$(window).scroll(chkScroll);


var tOut;
function expandBar(p) {
    var nav = document.getElementsByTagName('nav')[0];
    nav.style.top = nav.offsetTop;
    if (p == 0 && parseInt(nav.style.top) > -69) {
        nav.style.top = ( parseInt(nav.style.top) - 3 ) + "px";
        clearTimeout(tOut);
        tOut = setTimeout(expandBar, 13, 0);
    }
    else if (p == 1 && parseInt(nav.style.top) < 0) {
        nav.style.top = ( parseInt(nav.style.top) + 3 ) + "px";
        clearTimeout(tOut);
        tOut = setTimeout(expandBar, 13, 1);
    }
}

$('#xbox').click(function () {
    $('#popup').css('display', 'none');
});

$('#okBox').click(function () {
    $('#popup').css('display', 'none');
});

$('img').bind('contextmenu', function (e) {
    return false;
});

var showPopup = function (flag, message, callback) {
    $('#message').text(message);
    $('#popup').css('display', 'block');
    $('#select_ok').css('display', 'none');
    $('#select_bool').css('display', 'none');
    if (flag === 0) {
        $('#select_ok').css('display', 'block');
    } else if (flag === 1) {
        $('#select_bool').css('display', 'block');

        $('#yesBox').click(function () {
            $('#popup').css('display', 'none');
            if (callback != undefined) {
                $('#yesBox').unbind('click');
                callback(true);
            }
        });

        $('#noBox').click(function () {
            $('#popup').css('display', 'none');
            if (callback != undefined) {
                $('#noBox').unbind('click');
                callback(false);
            }
        });
    }
};

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

var curPage = location.href;
{
    if (curPage.search("recommend") > -1)
        curPage = 0;
    else if (curPage.search("browse") > -1)
        curPage = 1;
    else if (curPage.search("user") > -1)
        curPage = 2;
    else if (curPage.search("upload") > -1)
        curPage = 3;
    else if (curPage.search("flow") > -1)
        curPage = 4;
}

function curNavBtnActive() {
    $("nav .container :nth(" + curPage + ")").css("border-color", "#f9f8f8");
}
function curNavBtnDeactive() {
    $("nav .container :nth(" + curPage + ")").css("border-color", "transparent");
}

$("nav .container").children().bind("mouseout", curNavBtnActive);
$("nav .container").children().bind("mouseover", curNavBtnDeactive);
curNavBtnActive();

$(document).ready(function () {
    if ($('nav #signText').outerWidth() <= 40) {
        $('nav #signText').css('width', '40px');
        $('nav #logoNavButton').css('width', '44px');
    } else {
        $('nav #logoNavButton').css('width', $('#signText').outerWidth() + 4);
    }
    $('nav #logoNavButton').css('height', $('nav #signText').outerHeight() + 28);
    $('nav #signText').css('margin-left', '2px');
    $('nav #signText').css('margin-right', '2px');
    $('nav #signText').css('display', 'block');
});

