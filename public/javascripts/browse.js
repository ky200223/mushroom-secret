/**
 * Created by youngkim on 2014. 11. 26..
 */

var user = (function () {
    var userId;

    var get = function () {
        return userId;
    };

    var set = function (id) {
        userId = id;
    };

    return {
        get: get,
        set: set
    }
})();

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var subscribe = function (e) {
    var sendData = {
        user_id: user.get(),
        artist_id: e.target.parentNode.childNodes[2].innerText
    };

    $.ajax({
        type: "POST",
        async: true,
        url: '/user/subscribe',
        data: sendData,
        success: function () {
            var subscribeBtn = e.target;
            subscribeBtn.setAttribute('id', 'subscribed');
            subscribeBtn.setAttribute('src', '/images/subscribed.png');
            $(subscribeBtn).unbind('click');
            $(subscribeBtn).click(function (e) {
                e.stopPropagation();
                unSubscribe(e);
            });
        }
    });
};

var unSubscribe = function (e) {
    var sendData = {
        user_id: user.get(),
        artist_id: e.target.parentNode.childNodes[2].innerText
    };

    $.ajax({
        type: "POST",
        async: true,
        url: '/user/unSubscribe',
        data: sendData,
        success: function () {
            var unSubscribeBtn = e.target;
            unSubscribeBtn.setAttribute('id', 'subscribe');
            unSubscribeBtn.setAttribute('src', '/images/subscribe.png');
            $(unSubscribeBtn).unbind('click');
            $(unSubscribeBtn).click(function (e) {
                e.stopPropagation();
                subscribe(e);
            });
        }
    });
};

var like = function (e) {
    var sendData = {
        user_id: user.get(),
        artwork_id: e.target.parentNode.parentNode.childNodes[2].innerText
    };

    $.get('http://54.64.254.152/?user_id=' + user.get()
    + '&item_id=' + sendData.artwork_id + '&status=2', function () {
        var likeBtn = e.target;
        likeBtn.setAttribute('id', 'liked');
        likeBtn.setAttribute('src', '/images/browse_liked.png');
        $(likeBtn).unbind('click');
        $(likeBtn).click(function (e) {
            e.stopPropagation();
            unLike(e);
        });
    });
};

var unLike = function (e) {
    var sendData = {
        user_id: user.get(),
        artwork_id: e.target.parentNode.parentNode.childNodes[2].innerText
    };

    $.get('http://54.64.254.152/?user_id=' + user.get()
    + '&item_id=' + sendData.artwork_id + '&status=-1', function () {
        var likedBtn = e.target;
        likedBtn.setAttribute('id', 'like');
        likedBtn.setAttribute('src', '/images/browse_like.png');
        $(likedBtn).unbind('click');
        $(likedBtn).click(function (e) {
            e.stopPropagation();
            like(e);
        });
    });
};

var sendIndex = 0;
var sendMoreQuery = function (callback) {
    var searchFlag = getParameterByName('searchFlag');
    var searchText = $('#searchText').val();
    var sendUrl;

    if (searchText != '' && searchText != undefined) {
        sendUrl = '/browse/' + searchFlag + '/' + searchText;
    } else {
        sendUrl = '/browse/' + searchFlag
    }

    sendIndex = sendIndex + 18;
    var data = {
        sendIndex: sendIndex
    };

    $.ajax({
        type: "POST",
        async: true,
        url: sendUrl,
        data: data,
        success: function (data) {
            if (data.end) {
                $('#loadMore').css('display', 'none');
                $('#footerSpacer').css('display', 'block');
            }
            if (data.flag === 'artwork') {
                createArtworkCards(data.result);
            } else if (data.flag === 'artist') {
                createArtistCards(data.result);
            }
            callback();
        }
    }).fail(function () {
        sendIndex = sendIndex - 18;
    });
};

var createArtworkCards = function (dataArray) {
    dataArray.forEach(function (value) {
        createArtworkCard(value);
    });
};

var createArtworkCard = function (data) {
    var artworkFrag = document.createDocumentFragment();

    var artCard = document.createElement('div');
    artCard.classList.add('artCard');
    artCard.onclick = function () {
        window.location.href = '/user/page/' + data.artistId;
    };

    var artworkOverlay = document.createElement('div');
    artworkOverlay.classList.add('artworkOverlay');

    artCard.appendChild(artworkOverlay);

    var artworkInfo = document.createElement('div');
    artworkInfo.classList.add('artworkInfo');

    var introduction = document.createElement('div');
    introduction.classList.add('introduction');
    introduction.innerText = data.title;

    var nameLikeDiv = document.createElement('div');
    nameLikeDiv.classList.add('nameLikeDiv');

    var artistNickname = document.createElement('div');
    artistNickname.classList.add('artistNickname');
    artistNickname.innerText = data.nickname;

    var likeBtn = document.createElement('img');
    if (data.like_flag === 1) {
        likeBtn.setAttribute('src', '/images/browse_liked.png');
        likeBtn.classList.add('liked');
        $(likeBtn).click(function (e) {
            e.stopPropagation();
            unLike(e);
        });
    } else if (data.like_flag === 0) {
        likeBtn.setAttribute('src', '/images/browse_like.png');
        likeBtn.classList.add('like');
        $(likeBtn).click(function (e) {
            e.stopPropagation();
            like(e);
        });
    }

    var artworkId = document.createElement('div');
    artworkId.classList.add('artworkId');
    artworkId.innerText = data.id;

    artworkInfo.appendChild(introduction);

    nameLikeDiv.appendChild(artistNickname);
    nameLikeDiv.appendChild(likeBtn);

    artworkInfo.appendChild(nameLikeDiv);
    artworkInfo.appendChild(artworkId);

    artCard.appendChild(artworkInfo);

    var thumbnail = document.createElement('img');
    var imgSrc = 'http://do086lo4rikup.cloudfront.net/' + data.thumbnail_url;
    thumbnail.setAttribute('src', imgSrc);

    artCard.appendChild(thumbnail);

    artworkFrag.appendChild(artCard);

    document.getElementById('searchResult').appendChild(artworkFrag);
};

var createArtistCards = function (dataArray) {
    dataArray.forEach(function (value) {
        createArtistCard(value);
    });
};

var createArtistCard = function (data) {
    var artworkFrag = document.createDocumentFragment();

    var artistCard = document.createElement('div');
    artistCard.classList.add('artistCard');
    artistCard.onclick = function () {
        window.location.href = '/user/page/' + data.id;
    };

    var artistOverlay = document.createElement('div');
    artistOverlay.classList.add('artistOverlay');

    artistCard.appendChild(artistOverlay);

    var artistInfo = document.createElement('div');
    artistInfo.classList.add('artistInfo');

    var subscribeBtn = document.createElement('img');
    if (data.sub_id === 1) {
        subscribeBtn.setAttribute('src', '/images/subscribed.png');
        subscribeBtn.classList.add('subscribed');
        $(subscribeBtn).click(function (e) {
            e.stopPropagation();
            unSubscribe(e);
        });
    } else if (data.sub_id === 0) {
        subscribeBtn.setAttribute('src', '/images/subscribe.png');
        subscribeBtn.classList.add('subscribe');
        $(subscribeBtn).click(function (e) {
            e.stopPropagation();
            subscribe(e);
        });
    }

    var artistNickname = document.createElement('div');
    artistNickname.classList.add('artistNickname');
    artistNickname.innerText = data.nickname;

    var artistId = document.createElement('div');
    artistId.classList.add('artistId');
    artistId.innerText = data.id;

    artistInfo.appendChild(subscribeBtn);
    artistInfo.appendChild(artistNickname);
    artistInfo.appendChild(artistId);

    artistCard.appendChild(artistInfo);

    var thumbnail = document.createElement('img');
    if (data.profile_url === '0' || data.profile_url === 0) {
        var imgSrc = '/user_profile/uploadPrefix/profile.jpg';
    } else {
        var imgSrc = 'http://do086lo4rikup.cloudfront.net/' + data.profile_url;
    }
    thumbnail.setAttribute('src', imgSrc);

    artistCard.appendChild(thumbnail);

    artworkFrag.appendChild(artistCard);

    document.getElementById('searchResult').appendChild(artworkFrag);
};

var prevSearchText;
var sendInstantQuery = function () {
    var searchFlag = getParameterByName('searchFlag');
    var searchText = $('#searchText').val();
    var sendUrl;

    if (searchText != '' && searchText != undefined
        && searchText != prevSearchText && searchText != getParameterByName('searchText')) {
        sendUrl = '/browse/instant/' + searchFlag + '/' + searchText;

        $.ajax({
            type: "POST",
            async: true,
            url: sendUrl,
            success: function (data) {
                prevSearchText = searchText;

                $('#instantSearch').css('display', 'block');
                resetSearchField(function () {
                    fillInstantSearchField(data, function () {
                        adjustInstantSearchFieldWidth();
                    });
                });
            }
        }).fail(function () {
            console.log('fail');
        });
    } else if (searchText === '') {
        $('#instantSearch').css('display', 'none');
    }
};

var resetSearchField = function (callback) {
    $('#instantResult1').text('');
    $('#instantResult2').text('');
    $('#instantResult3').text('');
    $('#instantResult1').css('width', 'auto');
    $('#instantResult2').css('width', 'auto');
    $('#instantResult3').css('width', 'auto');

    callback();
};

var fillInstantSearchField = function (data, callback) {
    if (data.result1.length > 15)
        $('#instantResult1').text(data.result1.slice(0, 15) + '...');
    else
        $('#instantResult1').text(data.result1);
    var resultTextDiv1 = document.createElement('div');
    resultTextDiv1.classList.add('fullText');
    resultTextDiv1.innerText = data.result1;
    $('#instantResult1').append(resultTextDiv1);

    if (data.result2.length > 15)
        $('#instantResult2').text(data.result2.slice(0, 15) + '...');
    else
        $('#instantResult2').text(data.result2);
    var resultTextDiv2 = document.createElement('div');
    resultTextDiv2.classList.add('fullText');
    resultTextDiv2.innerText = data.result2;
    $('#instantResult2').append(resultTextDiv2);

    if (data.result3.length > 15)
        $('#instantResult3').text(data.result3.slice(0, 15) + '...');
    else
        $('#instantResult3').text(data.result3);
    var resultTextDiv3 = document.createElement('div');
    resultTextDiv3.classList.add('fullText');
    resultTextDiv3.innerText = data.result3;
    $('#instantResult3').append(resultTextDiv3);

    callback();
};

var adjustInstantSearchFieldWidth = function () {
    $('#instantResult1').css('width', $('#instantResult1').outerWidth() + 100);
    $('#instantResult1').css('marginLeft', (960 - $('#instantResult1').outerWidth()) / 2);
    $('#instantResult2').css('width', $('#instantResult2').outerWidth() + 100);
    $('#instantResult2').css('marginLeft', (960 - $('#instantResult2').outerWidth()) / 2);
    $('#instantResult3').css('width', $('#instantResult3').outerWidth() + 100);
    $('#instantResult3').css('marginLeft', (960 - $('#instantResult3').outerWidth()) / 2);
};

var instantResultClickSearch = function (e) {
    var searchDiv = e.target.childNodes[1];
    var searchFlag = getParameterByName('searchFlag');
    var searchText = $(searchDiv).text();

    window.location.href = '/browse/?searchFlag=' + searchFlag + '&searchText=' + searchText;
};

var searchIndex = 0;
var instantSearchKeyUp = function () {
    if (searchIndex === 0) {
        searchIndex = 3;
    } else if (searchIndex === 1) {
        searchIndex = 3;
    } else {
        searchIndex = searchIndex - 1;
    }

    instantSearchResultBorder();
};

var instantSearchKeyDown = function () {
    if (searchIndex === 0) {
        searchIndex = 1;
    } else if (searchIndex === 3) {
        searchIndex = 1;
    } else {
        searchIndex = searchIndex + 1;
    }

    instantSearchResultBorder();
};

var instantSearchResultBorder = function () {
    if (searchIndex === 1) {
        $('#instantResult1').css('border', '1px solid #2d2d2c');
        $('#instantResult2').css('border', '1px solid transparent');
        $('#instantResult3').css('border', '1px solid transparent');
        searchInfo.searchText = $('#instantResult1 .fullText').text();
    } else if (searchIndex === 2) {
        $('#instantResult2').css('border', '1px solid #2d2d2c');
        $('#instantResult1').css('border', '1px solid transparent');
        $('#instantResult3').css('border', '1px solid transparent');
        searchInfo.searchText = $('#instantResult2 .fullText').text();
    } else if (searchIndex === 3) {
        $('#instantResult3').css('border', '1px solid #2d2d2c');
        $('#instantResult1').css('border', '1px solid transparent');
        $('#instantResult2').css('border', '1px solid transparent');
        searchInfo.searchText = $('#instantResult3 .fullText').text();
    }
};

var searchInfo = {
    searchFlag: getParameterByName('searchFlag'),
    searchText: $('#searchText').val()
};
var sendSearchRequest = function () {
    if (searchIndex === 0) {
        searchInfo.searchText = $('#searchText').val();
    }
    if (searchInfo.searchText != '' && searchInfo.searchText != undefined) {
        window.location.href = '/browse/?searchFlag='
        + searchInfo.searchFlag + '&searchText=' + searchInfo.searchText;
    } else {
        window.location.href = '/browse/?searchFlag=' + searchInfo.searchFlag;
        //showPopup(0, 'please input search text');
    }
};

$(document).ready(function () {
    $.get('/user/getUserId', function (data) {
    }).done(function (data) {
        user.set(data.id);
    });

    $('#searchText').val(getParameterByName('searchText'));

    (function toggleBtn() {
        var artworkToggle = document.getElementById('artworkToggle');
        var artistToggle = document.getElementById('artistToggle');

        if (getParameterByName('searchFlag') === 'artwork') {
            artworkToggle.removeAttribute('src');
            artworkToggle.setAttribute('src', '/images/artwork_on.png');
        } else if (getParameterByName('searchFlag') === 'artist') {
            artistToggle.removeAttribute('src');
            artistToggle.setAttribute('src', '/images/artist_on.png');
        }
    })();

    $('#searchIcon img').click(function () {
        sendSearchRequest();
    });

    //to set arrow key up and down event
    var ignoreKey = false;
    var handler = function (e) {
        if (ignoreKey) {
            e.preventDefault();
            return;
        }
        if (e.keyCode == 38 || e.keyCode == 40) {
            ignoreKey = true;
            setTimeout(function () {
                ignoreKey = false
            }, 1);
            e.preventDefault();
        }
        if (e.keyCode == 38) {
            instantSearchKeyUp();
        } else if (e.keyCode == 40) {
            instantSearchKeyDown();
        }
    };

    document.getElementById('searchText').addEventListener('keydown', handler, false);
    document.getElementById('searchText').addEventListener('keypress', handler, false);

    $('#searchBox #searchText').keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            sendSearchRequest();
        }
    });

    $('#searchBox #searchText').click(function (e) {
        searchIndex = 0;
    });

    $('#searchResult .subscribe').click(function (e) {
        e.stopPropagation();
        subscribe(e);
    });

    $('#searchResult .subscribed').click(function (e) {
        e.stopPropagation();
        unSubscribe(e);
    });

    $('#searchResult .like').click(function (e) {
        like(e);
        e.stopPropagation();
    });

    $('#searchResult .liked').click(function (e) {
        unLike(e);
        e.stopPropagation();
    });

    $('#loadMore').click(function () {
        sendMoreQuery(function () {
            sendMore = true;
        });
    });

    sendInstantQuery();
    setInterval(sendInstantQuery, 1000);

    $('#instantResult1').click(function (e) {
        instantResultClickSearch(e);
    });

    $('#instantResult2').click(function (e) {
        instantResultClickSearch(e);
    });

    $('#instantResult3').click(function (e) {
        instantResultClickSearch(e);
    });
});

var sendMore = true;
$(window).scroll(function () {
    if ($(window).scrollTop() + window.innerHeight >= $(document).height() && $('#loadMore').css('display') !== 'none'
        && sendMore) {
        sendMore = false;
        sendMoreQuery(function () {
            sendMore = true;
        });
    }
});