/**
 * Created by youngkim on 2014. 11. 12..
 */

'use strict';

var validForm = (function () {
    var mailField = document.getElementById('email'),
        emailRegExp = /.+\@.+\..+/;

    var validEmailFormat = function () {
        return (emailRegExp.test(mailField.value));
    };

    return {
        validEmailFormat: validEmailFormat
    };
}());


var submitForm = (function () {
    var sendRequest = function () {
        if (validForm.validEmailFormat()) {
            document.forms['login-form'].submit();
        }
    };

    return {
        sendRequest: sendRequest
    }
})();

var submit = submitForm.sendRequest;

$( "#email" ).keypress(function( event ) {
    if ( event.which == 13 ) {
        event.preventDefault();
        submit();
    }
});

$( "#password" ).keypress(function( event ) {
    if ( event.which == 13 ) {
        event.preventDefault();
        submit();
    }
});

//$('#googlePlus').click(function () {
//    setCookie('googleplus', 'true', 30);
//});
//
//$('#facebook').click(function () {
//    setCookie('facebook', 'true', 30);
//    login();
//});
//
///*****************************
// * Google plus login
// *
// * 1. get google access token
// * 2. get user's data
// * 3. send data
// *****************************/
//(function () {
//    var po = document.createElement('script');
//    po.type = 'text/javascript';
//    po.async = true;
//    po.src = 'https://apis.google.com/js/client:plusone.js?onload=render';
//    var s = document.getElementsByTagName('script')[0];
//    s.parentNode.insertBefore(po, s);
//})();
//
//function render() {
//    gapi.signin.render('googlePlusBtn', {
//        'callback': 'googlePlusSignInCallback',
//        'clientid': '264201535369-qg1tbsucfkjujbr4un0ueki40b45hsq7.apps.googleusercontent.com',
//        'cookiepolicy': 'single_host_origin',
//        'requestvisibleactions': 'http://schemas.google.com/AddActivity',
//        'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
//    });
//}
//
//// 1. get google access token
//function googlePlusSignInCallback(authResult) {
//    // authentication success
//    if (authResult['access_token']) {
//        $('#googlePlusBtn').attr('style', 'cursor: default');
//
//        gapi.auth.setToken(authResult);     // store token
//
//        // 2. get Data
//        getEmailFromGoogleAPI(function (userData) {
//
//            var googleplus = getCookie('googleplus');
//
//            if (googleplus === 'true') {
//                // 3. send token
//                $.ajax({
//                    type: 'POST',
//                    url: '/user/login/google',
//                    contentType: 'application/json',
//                    success: function (result) {
//                        window.location.replace('http://mushroom.land/upload');
//                    },
//                    error: function (request, status, error) {
//                        console.error('Ajax error: googlePlusSignInCallback function.');
//                    },
//                    processData: false,
//                    data: JSON.stringify({
//                        userData: userData
//                        //googleOauth2Token: authResult['access_token']
//                    })
//                });
//            }
//        });
//    }
//    // authentication fail
//    else if (authResult['error']) {
//        if (authResult['error'] == 'access_denied')
//            console.log('사용자가 Google plus login 을 한다고 했다가 창이 뜨니깐 동의 하지 않았습니다.');
//        else if (authResult['error'] == 'immediate_failed')
//            console.log('사용자가 Google plus에 자동으로 로그인 할 수 없습니다.');
//    }
//
//    function getEmailFromGoogleAPI(callback) {
//        gapi.client.load('oauth2', 'v2', function () {
//            var request = gapi.client.oauth2.userinfo.get();
//            request.execute(getEmailCallback);
//        });
//
//        function getEmailCallback(obj) {
//            callback(obj);
//        }
//    }
//}
//
///*****************************
// * Facebook login
// *
// * 1. get facebook module
// * 2. get user's data
// * 3. send data
// *****************************/
//
//    // This is called with the results from from FB.getLoginStatus().
//function statusChangeCallback(response) {
//    // The response object is returned with a status field that lets the
//    // app know the current login status of the person.
//    // Full docs on the response object can be found in the documentation
//    // for FB.getLoginStatus().
//    if (response.status === 'connected') {
//        // Logged into your app and Facebook.
//        login();
//    } else if (response.status === 'not_authorized') {
//        // The person is logged into Facebook, but not your app.
//        console.log('사용자가 facebook Login 을 한다고 했다가 창이 뜨니깐 동의 하지 않았습니다.');
//    } else {
//        // The person is not logged into Facebook, so we're not sure if
//        // they are logged into this app or not.
//        console.log('사용자가 facebook에 자동으로 로그인 할 수 없습니다.');
//    }
//}
//
//function checkLoginState() {
//    FB.getLoginStatus(function (response) {
//        statusChangeCallback(response);
//    });
//}
//
//window.fbAsyncInit = function () {
//    FB.init({
//        appId: '606556316139036',
//        xfbml: true,
//        cookie: true,
//        version: 'v2.2'
//    });
//
//    FB.getLoginStatus(function (response) {
//        statusChangeCallback(response);
//    });
//};
//
//(function (d, s, id) {
//    var js, fjs = d.getElementsByTagName(s)[0];
//    if (d.getElementById(id)) {
//        return;
//    }
//    js = d.createElement(s);
//    js.id = id;
//    js.src = "//connect.facebook.net/en_US/sdk.js";
//    fjs.parentNode.insertBefore(js, fjs);
//}(document, 'script', 'facebook-jssdk'));
//
//function login() {
//    var facebook = getCookie('facebook');
//
//    if (facebook === 'true') {
//        FB.api('/me', function (response) {
//            $.ajax({
//                type: 'POST',
//                url: '/user/login/facebook',
//                contentType: 'application/json',
//                success: function (result) {
//                    window.location.replace('http://mushroom.land/upload');
//                },
//                error: function (request, status, error) {
//                    console.error('Ajax error: facebookLogin function.');
//                },
//                processData: false,
//                data: JSON.stringify({
//                    userData: response
//                })
//            });
//        });
//    }
//}