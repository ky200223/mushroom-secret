/**
 * Created by youngkim on 2014. 11. 18..
 */

'use strict';

var fullscreenDiv = $("#fullScreen");
fullscreenDiv.css({
    'position': 'fixed',
    'left': '0px',
    'top': '0px',
    'display': 'none',
    'box-sizing': 'border-box',
    'width': '100%',
    'height': '100%',
    'background': '#2d2d2c',
    'line-height': window.innerHeight + "px",
    'text-align': 'center'
});

var loadingDiv = $("#loadingScreen");
loadingDiv.css({
    'position': 'fixed',
    'left': '0px',
    'top': '0px',
    'display': 'none',
    'box-sizing': 'border-box',
    'width': '100%',
    'height': '100%',
    'background': '#2d2d2c',
    'line-height': window.innerHeight + "px",
    'text-align': 'center'
});

var loadMore = (function () {
    var loadMoreTime = 1;

    var get = function () {
        return loadMoreTime;
    };

    var increase = function () {
        loadMoreTime++;
    };

    return {
        get: get,
        increase: increase
    }
})();

var user = (function () {
    var userId;

    var get = function () {
        return userId;
    };

    var set = function (id) {
        userId = id;
    };

    return {
        get: get,
        set: set
    }
})();

var artworkList = (function () {
    var itemList = [];
    var usedItem = [];
    var index = 0;

    var getItem = function () {
        var requestItem = itemList.slice(index, index + 3);
        index = index + 3;
        usedItem.concat(requestItem);
        return requestItem;
    };

    var notDuplicate = function (element) {
        var i = itemList.length;
        while (i--)
            if (itemList[i] === element)
                return false;
        return true;
    };

    var empty = function () {
        return itemList.length <= index;
    };

    var set = function (array) {
        itemList = itemList.concat(array.filter(notDuplicate));
    };

    return {
        get: getItem,
        set: set,
        empty: empty
    }
})();

var getRecommendArtworkList = function (callback) {
    $.get('http://54.64.254.152/' + user.get(), function (response) {

    }).done(function (response) {
        artworkList.set(response.Items);
        callback();
    });
};

var getArtworkInfo = function (array, callback) {
    var images = {
        id_1: parseInt(array[0]),
        id_2: parseInt(array[1]),
        id_3: parseInt(array[2])
    };

//    if (array.length < 3) {
//        $('#loadMore').css('display', 'none');
//    }

    $.ajax({
        type: "POST",
        url: '/recommend/loadMore',
        data: images,
        success: function (response) {
            callback(response);
        }
    });
};

var showArtwork = function (data, callback) {
    var art1 = data.artwork_1 || undefined,
        art2 = data.artwork_2 || undefined,
        art3 = data.artwork_3 || undefined;

    createArtworkCard(art1, function () {
        createArtworkCard(art2, function () {
            createArtworkCard(art3, function () {
                callback();
            });
        });
    });

//    if (artworkList.empty()) {
//        $('#loadMore').css('display', 'none');
//    }
};

var likeButtonChange = function (e, sendRequest) {
    var likeBtn = e.target.parentNode.parentNode;
    var likeBtnParent = likeBtn.parentNode;
    var artworkId = likeBtnParent.childNodes[2].innerText;

    if (sendRequest === undefined || sendRequest === true) {
        $.get('http://54.64.254.152/?user_id=' + user.get()
        + '&item_id=' + artworkId + '&status=2', function (response) {
        }).done(function () {
            console.log('like complete');
        });
    }

    likeBtnParent.removeChild(likeBtn);

    var likedFragment = document.createDocumentFragment();

    var liked = document.createElement('div');
    liked.classList.add('liked');

    var likedIcon = document.createElement('div');
    likedIcon.classList.add('likedIcon');
    likedIcon.style.width = '30px';
    likedIcon.style.height = '30px';
    likedIcon.style.margin = '20px auto 0 auto';
    likedIcon.style.cursor = 'pointer';

    var likedImage = document.createElement('img');
    likedImage.setAttribute('src', '/images/liked.png');
    likedImage.style.width = '30px';
    likedImage.style.height = '30px';

    likedIcon.appendChild(likedImage);
    likedIcon.addEventListener('click', likedButtonChange);
    liked.appendChild(likedIcon);

    var likedText = document.createElement('div');
    likedText.classList.add('likedText');
    likedText.innerText = 'LIKED';
    likedText.style.width = '30px';
    likedText.style.height = '11px';
    likedText.style.margin = '10px auto 100px auto';
    likedText.style.textAlign = 'center';
    likedText.style.fontFamily = 'Century gothic, Noto Sans';
    likedText.style.fontSize = '11px';
    likedText.style.fontWeight = '700';
    likedText.style.letterSpacing = '0.5px';

    liked.appendChild(likedText);
    likedFragment.appendChild(liked);
    likeBtnParent.appendChild(likedFragment);
};

var likedButtonChange = function (e, sendRequest) {
    var likedBtn = e.target.parentNode.parentNode;
    var likedBtnParent = likedBtn.parentNode;
    var artworkId = likedBtnParent.childNodes[2].innerText;

    if (sendRequest === undefined || sendRequest === true) {
        $.get('http://54.64.254.152/?user_id=' + user.get()
        + '&item_id=' + artworkId + '&status=-1', function (response) {
        }).done(function () {
            console.log('unliked complete');
        });
    }

    likedBtnParent.removeChild(likedBtn);

    var likeFragment = document.createDocumentFragment();

    var like = document.createElement('div');
    like.classList.add('like');

    var likeIcon = document.createElement('div');
    likeIcon.classList.add('likeIcon');
    likeIcon.style.width = '30px';
    likeIcon.style.height = '30px';
    likeIcon.style.margin = '20px auto 0 auto';
    likeIcon.style.cursor = 'pointer';

    var likeImage = document.createElement('img');
    likeImage.setAttribute('src', '/images/like.png');
    likeImage.style.width = '30px';
    likeImage.style.height = '30px';

    likeIcon.appendChild(likeImage);
    likeIcon.addEventListener('click', likeButtonChange);
    like.appendChild(likeIcon);

    var likeText = document.createElement('div');
    likeText.classList.add('likeText');
    likeText.innerText = 'LIKE';
    likeText.style.width = '30px';
    likeText.style.height = '11px';
    likeText.style.margin = '10px auto 100px auto';
    likeText.style.textAlign = 'center';
    likeText.style.fontFamily = 'Century gothic, Noto Sans';
    likeText.style.fontSize = '11px';
    likeText.style.fontWeight = '700';
    likeText.style.letterSpacing = '0.5px';

    like.appendChild(likeText);
    likeFragment.appendChild(like);
    likedBtnParent.appendChild(likeFragment);
};

var showFullScreen = function (e, image) {
    $.get('http://54.64.254.152/?user_id=' + user.get()
    + '&item_id=' + image.id + '&status=1', function () {
    });

    $(window).on("mousewheel.disableScroll DOMMouseScroll.disableScroll touchmove.disableScroll", function (e) {
        e.preventDefault();
    });

    var originalLikeBtn = e.parentNode.parentNode.childNodes[3].childNodes[0].childNodes[0];
    if ($(originalLikeBtn).attr('src') === '/images/like.png') {
        $('#fullScreen #likeButton img').attr('src', '/images/full_like.png');
        $('#fullScreen #likeButton').click(function () {
            $.get('http://54.64.254.152/?user_id=' + user.get()
            + '&item_id=' + image.id + '&status=2', function () {
            }).done(function () {
                console.log('like complete')
            });

            $('#fullScreen #likeButton img').attr('src', '/images/full_liked.png');

            var likeButton = {};
            likeButton.target = e.parentNode.parentNode.childNodes[3].childNodes[0].childNodes[0];
            likeButtonChange(likeButton, false);
        });
    } else {
        $('#fullScreen #likeButton img').attr('src', '/images/full_liked.png');
        $('#fullScreen #likeButton').click(function () {
            $.get('http://54.64.254.152/?user_id=' + user.get()
            + '&item_id=' + image.id + '&status=-1', function () {
            }).done(function () {
                console.log('unlike complete')
            });

            $('#fullScreen #likeButton img').attr('src', '/images/full_like.png');

            var likeButton = {};
            likeButton.target = e.parentNode.parentNode.childNodes[3].childNodes[0].childNodes[0];
            likedButtonChange(likeButton, false);
        });
    }

    var cloneImage = e.cloneNode(true);
    cloneImage.setAttribute('id', 'fullArtwork');
    cloneImage.removeAttribute('style');

    var fullscreen = document.getElementById('fullScreen');
    fullscreen.appendChild(cloneImage);

    $('#fullScreen #moreButton').click(function () {
        var artistId = e.parentNode.parentNode.childNodes[1].innerText;
        window.location.href = 'http://mushroom.land/user/page/' + artistId;
    });

    var windowRatio = (window.innerWidth) / (window.innerHeight);
    var imageRatio = parseFloat(e.offsetWidth) / parseFloat(e.offsetHeight);

    if (windowRatio > imageRatio) {
        $('#fullArtwork').removeAttr("style");
        $('#fullArtwork').css('height', window.innerHeight);
        $('#fullArtwork').css('width', window.innerHeight * imageRatio);
        $('#fullArtwork').css('position', 'fixed');
        $('#fullArtwork').css('top', '0');
        $('#fullArtwork').css('left', '50%');
        $('#fullArtwork').css('margin-left', parseFloat($('#fullArtwork').css('width')) * (-0.5));
    } else if (windowRatio < imageRatio) {
        $('#fullArtwork').removeAttr("style");
        $('#fullArtwork').css('width', window.innerWidth);
        $('#fullArtwork').css('height', window.innerWidth / imageRatio);
        $('#fullArtwork').css('position', 'fixed');
        $('#fullArtwork').css('top', '50%');
        $('#fullArtwork').css('left', '0');
        $('#fullArtwork').css('margin-top', parseFloat($('#fullArtwork').css('height')) * (-0.5));
    } else {
        $('#fullArtwork').removeAttr("style");
        $('#fullArtwork').css('top', '0');
        $('#fullArtwork').css('left', '0');
        $('#fullArtwork').css('height', window.innerHeight);
        $('#fullArtwork').css('width', window.innerWidth);
    }

    $(window).resize(function () {
        var windowRatio = (window.innerWidth) / (window.innerHeight);

        if (windowRatio > imageRatio) {
            $('#fullArtwork').removeAttr("style");
            $('#fullArtwork').css('height', window.innerHeight);
            $('#fullArtwork').css('width', window.innerHeight * imageRatio);
            $('#fullArtwork').css('position', 'fixed');
            $('#fullArtwork').css('top', '0');
            $('#fullArtwork').css('left', '50%');
            $('#fullArtwork').css('margin-left', parseFloat($('#fullArtwork').css('width')) * (-0.5));
        } else if (windowRatio < imageRatio) {
            $('#fullArtwork').removeAttr("style");
            $('#fullArtwork').css('width', window.innerWidth);
            $('#fullArtwork').css('height', window.innerWidth / imageRatio);
            $('#fullArtwork').css('position', 'fixed');
            $('#fullArtwork').css('top', '50%');
            $('#fullArtwork').css('left', '0');
            $('#fullArtwork').css('margin-top', parseFloat($('#fullArtwork').css('height')) * (-0.5));
        } else {
            $('#fullArtwork').removeAttr("style");
            $('#fullArtwork').css('top', '0');
            $('#fullArtwork').css('left', '0');
            $('#fullArtwork').css('height', window.innerHeight);
            $('#fullArtwork').css('width', window.innerWidth);
        }
    });

    $('#fullScreen').click(function () {
        closeFullScreen();
    });

    $('#fullScreen #likeButton').click(function (e) {
        e.stopPropagation();
    });

    $('#fullScreen #moreButton').click(function (e) {
        e.stopPropagation();
    });

    $('img').bind('contextmenu', function () {
        return false;
    });

    fullscreenDiv.css('display', 'block');
};

var closeFullScreen = function () {
    var fullscreen = document.getElementById('fullScreen');
    var fullImage = fullscreen.childNodes[2];

    fullscreen.removeChild(fullImage);

    $('#fullScreen #likeButton img').attr('src', '/images/full_like.png');

    $('#fullScreen #likeButton').unbind('click');
    $('#fullScreen').unbind('click');
    $(window).unbind('resize');

    fullscreenDiv.css('display', 'none');

    $(window).off("mousewheel.disableScroll DOMMouseScroll.disableScroll touchmove.disableScroll");
};

var createArtworkCard = function (image, callback) {
    if (image === undefined || image === '') {
        callback();
    } else {
        $.get('http://54.64.254.152/?user_id=' + user.get()
        + '&item_id=' + image.id + '&status=0', function () {
        });

        var imgSrc = 'http://do086lo4rikup.cloudfront.net/' + image.photo_url;

        var artFragment = document.createDocumentFragment();

        var artCard = document.createElement('div');
        artCard.classList.add('artCard');
        $(artCard).css("display", "none");

        var artwork = document.createElement('div');
        artwork.classList.add('artwork');
//        artwork.style.width = '1000px';
//        artwork.style.height = '472px';
        artwork.style.margin = 'auto';
        artwork.style.textAlign = 'center';

        var artworkImg = document.createElement('img');
        artworkImg.setAttribute('src', imgSrc);
        artworkImg.setAttribute('align', 'center');
//        artworkImg.style.maxWidth = '1000px';
//        artworkImg.style.maxHeight = '472px';
        artworkImg.style.margin = 'auto';
        artworkImg.style.border = '1px solid #2d2d2c';

        var like = document.createElement('div');
        like.classList.add('like');

        var likeIcon = document.createElement('div');
        likeIcon.classList.add('likeIcon');
        likeIcon.style.width = '30px';
        likeIcon.style.height = '30px';
        likeIcon.style.margin = '20px auto 0 auto';
        likeIcon.style.cursor = 'pointer';

        var likeImage = document.createElement('img');
        likeImage.setAttribute('src', '/images/like.png');
        likeImage.style.width = '30px';
        likeImage.style.height = '30px';

        var likeText = document.createElement('div');
        likeText.classList.add('likeText');
        likeText.innerText = 'LIKE';
        likeText.style.width = '30px';
        likeText.style.height = '11px';
        likeText.style.margin = '10px auto 100px auto';
        likeText.style.textAlign = 'center';
        likeText.style.fontFamily = 'Century gothic, Noto Sans';
        likeText.style.fontSize = '11px';
        likeText.style.fontWeight = '700';
        likeText.style.letterSpacing = '0.5px';

        var artworkId = document.createElement('div');
        artworkId.classList.add('artworkId');
        artworkId.innerText = image.id;
        artworkId.style.display = 'none';

        var artist = document.createElement('div');
        artist.classList.add('artistId');
        artist.innerText = image.artist;
        artist.style.display = 'none';


        $(artworkImg).click(function (e) {
            showFullScreen(e.target, image);
        });

        $('img').bind('contextmenu', function () {
            return false;
        });

        $(artworkImg).load(function () {

            artwork.appendChild(artworkImg);
            artCard.appendChild(artwork);

            likeIcon.appendChild(likeImage);
            likeIcon.addEventListener('click', likeButtonChange);
            like.appendChild(likeIcon);

            artCard.appendChild(artist);
            artCard.appendChild(artworkId);

            like.appendChild(likeText);
            artCard.appendChild(like);
            artFragment.appendChild(artCard);

            resizeArtWorks();

            $('main .container .artworkSpace').append(artFragment);
        });

        callback();
    }
};

var artWorkWidth, artWorkHeight, artWorkRatio;
var resizeArtWork = function (obj) {
    var img = $(obj).children();
    var imgRatio = img[0].width / img[0].height;

    if (artWorkRatio > imgRatio) {
        $(obj).width(artWorkHeight * imgRatio);
        $(obj).height(artWorkHeight);
        img.width(artWorkHeight * imgRatio);
        img.height(artWorkHeight);
    }
    else {
        $(obj).width(artWorkWidth);
        $(obj).height(artWorkWidth / imgRatio);
        img.width(artWorkWidth);
        img.height(artWorkWidth / imgRatio);
    }
    var card = $(obj).parent();
    var padding = (artWorkHeight + 220 - card.height()) / 2;
    card.css("padding-top", padding);
    card.css("padding-bottom", padding);
    card.css("display", "block");
};

var resizeArtWorks = function () {
    artWorkWidth = $(window).width() - 280;
    artWorkHeight = window.innerHeight - 220;
    artWorkRatio = artWorkWidth / artWorkHeight;
    $.each($(".artwork"), function (idx, obj) {
        resizeArtWork(obj);
    });
};

$(document).ready(function () {
    $('#loadMore').css('display', 'none');

    $('.likeIcon').click(function (e) {
        likeButtonChange(e);
    });

    $('.likedIcon').click(function (e) {
        likedButtonChange(e);
    });

    artWorkWidth = $(window).width() - 280;
    artWorkHeight = window.innerHeight - 220;
    artWorkRatio = artWorkWidth / artWorkHeight;

    $.get('/user/getUserId', function (data) {
    }).done(function (data) {
        user.set(data.id);
        getRecommendArtworkList(function () {
            getArtworkInfo(artworkList.get(), function (artwork) {
                showArtwork(artwork, function () {
                    $('#loadMore').css('display', 'block');
                });
            });
        });
    });
});

$("#loadMore").click(function () {
//    loadMore.increase();
//    if (loadMore.get() !== 0 && loadMore.get() % 3 === 0) {
//        getRecommendArtworkList(function () {
//            getArtworkInfo(artworkList.get(), function (artwork) {
//                showArtwork(artwork, function () {
//                    console.log('load');
//                });
//            });
//        });
//    } else {
//        getArtworkInfo(artworkList.get(), function (artwork) {
//            showArtwork(artwork, function () {
//                console.log('load');
//            });
//        });
//    }
    getRecommendArtworkList(function () {
        getArtworkInfo(artworkList.get(), function (artwork) {
            showArtwork(artwork, function () {
                console.log('load');
            });
        });
    });
});

$(window).scroll(function () {
    if ($(window).scrollTop() + window.innerHeight >= $(document).height() && $('#loadMore').css('display') !== 'none') {
        $('#loadMore').css('display', 'none');
//        loadMore.increase();
//        if (loadMore.get() !== 0 && loadMore.get() % 3 === 0) {
//            console.log('more!');
//            getRecommendArtworkList(function () {
//                getArtworkInfo(artworkList.get(), function (artwork) {
//                    showArtwork(artwork, function () {
//                        console.log('load');
//                    });
//                });
//            });
//        } else {
//            console.log('use');
//            getArtworkInfo(artworkList.get(), function (artwork) {
//                showArtwork(artwork, function () {
//                    console.log('load');
//                });
//            });
//        }
        getRecommendArtworkList(function () {
            getArtworkInfo(artworkList.get(), function (artwork) {
                showArtwork(artwork, function () {
                    $('#loadMore').css('display', 'block');
                    console.log('loadScroll');
                });
            });
        });
    }
});

$(window).resize(resizeArtWorks);