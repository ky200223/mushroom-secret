/**
 * Created by youngkim on 2014. 11. 20..
 */

var imageRatio;

$('#fullArtwork').load(function () {
    var windowRatio = (window.innerWidth) / (window.innerHeight);
    imageRatio = parseFloat($('#fullArtwork').css('width')) /
        parseFloat($('#fullArtwork').css('height'));
    if (windowRatio > imageRatio) {
        $('#fullArtwork').removeAttr("style");
        $('#fullArtwork').css('height', window.innerHeight);
        $('#fullArtwork').css('width', window.innerHeight * imageRatio);
        $('#fullArtwork').css('position', 'fixed');
        $('#fullArtwork').css('top', '0');
        $('#fullArtwork').css('left', '50%');
        $('#fullArtwork').css('margin-left', parseFloat($('#fullArtwork').css('width')) * (-0.5));
    } else if (windowRatio < imageRatio) {
        $('#fullArtwork').removeAttr("style");
        $('#fullArtwork').css('width', window.innerWidth);
        $('#fullArtwork').css('height', window.innerWidth / imageRatio);
        $('#fullArtwork').css('position', 'fixed');
        $('#fullArtwork').css('top', '50%');
        $('#fullArtwork').css('left', '0');
        $('#fullArtwork').css('margin-top', parseFloat($('#fullArtwork').css('height')) * (-0.5));
    } else {
        $('#fullArtwork').removeAttr("style");
        $('#fullArtwork').css('top', '0');
        $('#fullArtwork').css('left', '0');
        $('#fullArtwork').css('height', window.innerHeight);
        $('#fullArtwork').css('width', window.innerWidth);
    }
    $('img').bind('contextmenu', function (e) {
        return false;
    });
});

$(window).resize(function () {
    var windowRatio = (window.innerWidth) / (window.innerHeight);
    if (windowRatio > imageRatio) {
        $('#fullArtwork').removeAttr("style");
        $('#fullArtwork').css('height', window.innerHeight);
        $('#fullArtwork').css('width', window.innerHeight * imageRatio);
        $('#fullArtwork').css('position', 'fixed');
        $('#fullArtwork').css('top', '0');
        $('#fullArtwork').css('left', '50%');
        $('#fullArtwork').css('margin-left', parseFloat($('#fullArtwork').css('width')) * (-0.5));
    } else if (windowRatio < imageRatio) {
        $('#fullArtwork').removeAttr("style");
        $('#fullArtwork').css('width', window.innerWidth);
        $('#fullArtwork').css('height', window.innerWidth / imageRatio);
        $('#fullArtwork').css('position', 'fixed');
        $('#fullArtwork').css('top', '50%');
        $('#fullArtwork').css('left', '0');
        $('#fullArtwork').css('margin-top', parseFloat($('#fullArtwork').css('height')) * (-0.5));
    } else {
        $('#fullArtwork').removeAttr("style");
        $('#fullArtwork').css('top', '0');
        $('#fullArtwork').css('left', '0');
        $('#fullArtwork').css('height', window.innerHeight);
        $('#fullArtwork').css('width', window.innerWidth);
    }
});

$('#fullBox').click(function () {
    window.location.href = document.referrer;
});

$('#fullArtwork').click(function (e) {
    e.stopPropagation();
});

$('#exitButton').click(function () {
    window.history.back();
});

$('#likeButton').click(function () {
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    $.get('http://www.corsproxy.com/54.65.86.166/?user_id=' + getParameterByName('user')
        + '&item_id=' + getParameterByName('artwork') + '&status=2', function () {
    });
});