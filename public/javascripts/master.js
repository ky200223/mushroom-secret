/**
 * Created by youngkim on 2014. 11. 22..
 */

var user = (function () {
    var userId;

    var get = function () {
        return userId;
    };

    var set = function (id) {
        userId = id;
    };

    return {
        get: get,
        set: set
    }
})();

$('#otherArtwork .artCard img').click(function (e) {
    var artworkId = e.target.parentNode.childNodes[1].childNodes[0].innerText;

    $.get('/artwork/get/' + artworkId + '/?userId=' + user.get(), function (data) {
    }).done(function (data) {
        changePicture(data.artwork, function () {
            changeArtworkInfo(data.artwork, function () {
                $('body,html').animate({
                    scrollTop: $('#userSpacer').position().top
                }, 800);
                return false;
            });
        });

        if (data.artwork.comment != null && data.artwork.comment != undefined) {
            changeCommentInfo(data.artwork.comment, function () {
            });
        } else {
            deleteCommentField(function () {
            });
        }
    });
});

var changePicture = function (data, callback) {
    var artworkImage = document.getElementById('artworkImage');

    var artFragment = document.createDocumentFragment();

    var newArtworkImg = document.createElement('img');
    newArtworkImg.setAttribute('src', 'http://do086lo4rikup.cloudfront.net/' + data.photo_url);
    newArtworkImg.style.maxWidth = '960px';
    newArtworkImg.style.maxHeight = '472px';
    newArtworkImg.style.margin = '0 auto';
    newArtworkImg.style.border = '1px solid #2d2d2c';

    artFragment.appendChild(newArtworkImg);

    artworkImage.removeChild(artworkImage.childNodes[0]);
    artworkImage.appendChild(artFragment);

    callback();
};

var likeButtonChange = function (e) {
    var likeBtn = e.target.parentNode.parentNode;
    var likeBtnParent = likeBtn.parentNode;
    var artworkId = $('#artworkId').text();

    $.get('http://54.64.254.152/?user_id=' + user.get()
    + '&item_id=' + artworkId + '&status=2', function (response) {
    });

    likeBtnParent.removeChild(likeBtn);

    var likedFragment = document.createDocumentFragment();

    var liked = document.createElement('div');
    liked.classList.add('liked');

    var likedIcon = document.createElement('div');
    likedIcon.classList.add('likedIcon');
    likedIcon.style.width = '50px';
    likedIcon.style.height = '50px';
    likedIcon.style.margin = '40px auto 0 auto';
    likedIcon.style.cursor = 'pointer';

    var likedImage = document.createElement('img');
    likedImage.setAttribute('src', '/images/liked.png');
    likedImage.style.width = '50px';

    likedIcon.appendChild(likedImage);
    likedIcon.addEventListener('click', likedButtonChange);
    liked.appendChild(likedIcon);

    var likedText = document.createElement('div');
    likedText.classList.add('likedText');
    likedText.innerText = 'LIKED';
    likedText.style.width = '50px';
    likedText.style.height = '15px';
    likedText.style.margin = '10px auto 40px auto';
    likedText.style.textAlign = 'center';
    likedText.style.fontFamily = 'Century gothic, Noto Sans';
    likedText.style.fontSize = '15px';
    likedText.style.fontWeight = '700';
    likedText.style.letterSpacing = '0.5px';

    liked.appendChild(likedText);
    likedFragment.appendChild(liked);
    likeBtnParent.insertBefore(likedFragment, likeBtnParent.childNodes[1]);
};

var likedButtonChange = function (e) {
    var likedBtn = e.target.parentNode.parentNode;
    var likedBtnParent = likedBtn.parentNode;
    var artworkId = $('#artworkId').text();

    $.get('http://54.64.254.152/?user_id=' + user.get()
    + '&item_id=' + artworkId + '&status=-1', function (response) {
    });

    likedBtnParent.removeChild(likedBtn);

    var likeFragment = document.createDocumentFragment();

    var like = document.createElement('div');
    like.classList.add('like');

    var likeIcon = document.createElement('div');
    likeIcon.classList.add('likeIcon');
    likeIcon.style.width = '50px';
    likeIcon.style.height = '50px';
    likeIcon.style.margin = '40px auto 0 auto';
    likeIcon.style.cursor = 'pointer';

    var likeImage = document.createElement('img');
    likeImage.setAttribute('src', '/images/like.png');
    likeImage.style.width = '50px';

    likeIcon.appendChild(likeImage);
    likeIcon.addEventListener('click', likeButtonChange);
    like.appendChild(likeIcon);

    var likeText = document.createElement('div');
    likeText.classList.add('likeText');
    likeText.innerText = 'LIKE';
    likeText.style.width = '50px';
    likeText.style.height = '15px';
    likeText.style.margin = '10px auto 40px auto';
    likeText.style.textAlign = 'center';
    likeText.style.fontFamily = 'Century gothic, Noto Sans';
    likeText.style.fontSize = '15px';
    likeText.style.fontWeight = '700';
    likeText.style.letterSpacing = '0.5px';

    like.appendChild(likeText);
    likeFragment.appendChild(like);
    likedBtnParent.insertBefore(likeFragment, likedBtnParent.childNodes[1]);
};

var createLikeButton = function (data) {
    var like = data.like;
    var oldButtonParent = document.getElementById('artwork');
    var oldButton = oldButtonParent.childNodes[1];

    if (like) {
        oldButtonParent.removeChild(oldButton);

        var likedFragment = document.createDocumentFragment();

        var liked = document.createElement('div');
        liked.classList.add('liked');

        var likedIcon = document.createElement('div');
        likedIcon.classList.add('likedIcon');
        likedIcon.style.width = '50px';
        likedIcon.style.height = '50px';
        likedIcon.style.margin = '40px auto 0 auto';
        likedIcon.style.cursor = 'pointer';

        var likedImage = document.createElement('img');
        likedImage.setAttribute('src', '/images/liked.png');
        likedImage.style.width = '50px';

        likedIcon.appendChild(likedImage);
        likedIcon.addEventListener('click', likedButtonChange);
        liked.appendChild(likedIcon);

        var likedText = document.createElement('div');
        likedText.classList.add('likedText');
        likedText.innerText = 'LIKED';
        likedText.style.width = '50px';
        likedText.style.height = '15px';
        likedText.style.margin = '10px auto 40px auto';
        likedText.style.textAlign = 'center';
        likedText.style.fontFamily = 'Century gothic, Noto Sans';
        likedText.style.fontSize = '15px';
        likedText.style.fontWeight = '700';
        likedText.style.letterSpacing = '0.5px';

        liked.appendChild(likedText);
        likedFragment.appendChild(liked);
        oldButtonParent.insertBefore(likedFragment, oldButtonParent.childNodes[1]);
    } else {
        oldButtonParent.removeChild(oldButton);

        var likeFragment = document.createDocumentFragment();

        var like = document.createElement('div');
        like.classList.add('like');

        var likeIcon = document.createElement('div');
        likeIcon.classList.add('likeIcon');
        likeIcon.style.width = '50px';
        likeIcon.style.height = '50px';
        likeIcon.style.margin = '40px auto 0 auto';
        likeIcon.style.cursor = 'pointer';

        var likeImage = document.createElement('img');
        likeImage.setAttribute('src', '/images/like.png');
        likeImage.style.width = '50px';

        likeIcon.appendChild(likeImage);
        likeIcon.addEventListener('click', likeButtonChange);
        like.appendChild(likeIcon);

        var likeText = document.createElement('div');
        likeText.classList.add('likeText');
        likeText.innerText = 'LIKE';
        likeText.style.width = '50px';
        likeText.style.height = '15px';
        likeText.style.margin = '10px auto 40px auto';
        likeText.style.textAlign = 'center';
        likeText.style.fontFamily = 'Century gothic, Noto Sans';
        likeText.style.fontSize = '15px';
        likeText.style.fontWeight = '700';
        likeText.style.letterSpacing = '0.5px';

        like.appendChild(likeText);
        likeFragment.appendChild(like);
        oldButtonParent.insertBefore(likeFragment, oldButtonParent.childNodes[1]);
    }
};

var changeArtworkInfo = function (data, callback) {
    if (data.title === '') {
        $('#title').text('No Title');
    } else {
        $('#title').text(data.title);
    }

    $('#width').text(data.width);
    $('#height').text(data.height);
    $('#depth').text(data.depth);
    $('#material').text(data.material);
    $('#artworkIntroduction').text(data.introduction);
    $('#artworkId').text(data.id);

    for (var i = 0; i < 5; i++) {
        $('.hash').eq(i).text('');
        $('.hash').eq(i).unbind('click');
        if (data.hashtag[i] != undefined && data.hashtag[i].tag != '') {
            var index = i;
            $('.hash').eq(i).click(function () {
                console.log(data.hashtag[index].tag);
                window.location.href = '/browse/?searchFlag=artwork&searchText=' + data.hashtag[index].tag;
            });

            $('.hash').eq(i).text('#' + data.hashtag[i].tag);
        }
    }

    createLikeButton(data);

    callback();
};

var changeCommentInfo = function (data, callback) {
    deleteCommentField(function () {
        data.forEach(function (value) {
            var data1 = {
                commentContent: value.content
            };
            var data2 = {
                commenterNickname: value.nickname,
                time: value.comment_time
            };
            addComment(data1, data2);
        });
        callback();
    });
};

var deleteCommentField = function (callback) {
    var commentList = document.getElementById('commentList');
    while (commentList.firstChild) {
        commentList.removeChild(commentList.firstChild)
    }
    callback();
};

var changeCommentNicknameMargin = function () {
    var commentNickname = document.getElementsByClassName('commentNickname');
    for (var i = 0; i < commentNickname.length; i++) {
        var commentContentField = commentNickname[i].parentNode.parentNode.childNodes[1];
        var commentHeight = commentContentField.offsetHeight;
        commentNickname[i].style.marginBottom = (commentHeight - 30) + 'px';
    }
};

var setCommentCardWidth = function () {
    var commentCard = document.getElementsByClassName('commentCard');
    for (var i = 0; i < commentCard.length; i++) {
        var commentContentField = commentCard[i].childNodes[1].childNodes[0];
        var commentHeight = commentContentField.offsetHeight;
        commentCard[i].style.height = (commentHeight + 20) + 'px';
        commentCard[i].style.marginBottom = '12px';
    }
};

var sendComment = function () {
    var artworkId = $('#artworkInfo #artworkId').text();

    var commentContent = $('#commentForm #commentInput').val();
    var commentData = {
        artwork_id: artworkId,
        commentContent: commentContent
    };

    if (commentContent === '' || commentContent === undefined) {
        showPopup(0, 'please input comment content');
    } else {
        $.ajax({
            type: "POST",
            async: true,
            url: '/artwork/comment/add',
            data: commentData,
            beforeSend: function () {
                $('#commentForm #commentInput').val('');
            },
            success: function (data) {
                addComment(commentData, data);
            },
            fail: function () {
                $('#commentForm #commentInput').val(commentContent);
            }
        });
    }
};

var addComment = function (comment, returnData) {
    var commentList = document.getElementById('commentList');

    var content = comment.commentContent;
    var commenterNickname = returnData.commenterNickname;
    var commentTime = returnData.time;

    var commentFrag = document.createDocumentFragment();

    var commentCard = document.createElement('div');
    commentCard.classList.add('commentCard');
    commentCard.style.width = '872px';

    var commentInfo = document.createElement('div');
    commentInfo.classList.add('commentInfo');
    commentInfo.style.float = 'left';
    commentInfo.style.width = '220px';
    commentInfo.style.height = 'auto';
    commentInfo.style.fontFamily = 'Century gothic, Noto Sans';
    commentInfo.style.fontSize = '15px';
    commentInfo.style.fontWeight = '500';
    commentInfo.style.color = '#2d2d2c';

    var commentNickname = document.createElement('div');
    commentNickname.classList.add('commentNickname');
    commentNickname.innerText = commenterNickname;
    commentNickname.style.textAlign = 'left';
    commentNickname.addEventListener('click', function () {
        window.location.href = '/user/page/' + returnData.user_id;
    });

    var commentDate = document.createElement('div');
    commentDate.classList.add('commentDate');
    if (commentTime.length >= 9) {
        commentDate.innerText = commentTime.slice(0, 10).split('-')[0]
        + commentTime.slice(0, 10).split('-')[1] + commentTime.slice(0, 10).split('-')[2];
    } else {
        commentDate.innerText = commentTime;
    }
    commentDate.style.textAlign = 'right';

    var commentId = document.createElement('div');
    commentId.classList.add('commentId');
    commentId.style.display = 'none';
    commentId.innerText = returnData.commentId;

    commentInfo.appendChild(commentNickname);
    commentInfo.appendChild(commentDate);
    commentInfo.appendChild(commentId);

    var commentContentField = document.createElement('div');
    commentContentField.classList.add('commentContentField');
    commentContentField.style.float = 'left';
    commentContentField.style.width = '593px';
    commentContentField.style.height = 'auto';
    commentContentField.style.margin = '0 0 12px 35px';
    commentContentField.style.padding = '10px 10px 10px 12px';
    commentContentField.style.border = '1px solid #2d2d2c';
    commentContentField.style.backgroundColor = '#2d2d2c';
    commentContentField.style.fontFamily = 'Georgia, Noto Serif';
    commentContentField.style.fontStyle = 'italic';
    commentContentField.style.fontSize = '15px';
    commentContentField.style.fontWeight = '500';
    commentContentField.style.color = '#f9f9f8';

    var commentContent = document.createElement('div');
    commentContent.classList.add('commentContent');
    commentContent.style.float = 'left';
    commentContent.style.width = '527px';
    commentContent.style.wordBreak = 'break-all';
    commentContent.style.lineHeight = '20px';
    commentContent.innerText = content;

    var commentControl = document.createElement('div');
    commentControl.classList.add('commentControl');

    if ($('#comment #curUserNickname').text() === commenterNickname) {
        var commentControl_x = document.createElement('img');
        commentControl_x.classList.add('x');
        commentControl_x.setAttribute('src', '/images/whitex.png');
        commentControl_x.style.float = 'right';
        commentControl_x.style.margin = '4px 0 0 12px';
        commentControl_x.style.cursor = 'pointer';
        commentControl_x.addEventListener('click', deleteComment);

        commentControl.appendChild(commentControl_x);

        var commentControl_edit = document.createElement('img');
        commentControl_edit.classList.add('commentEdit');
        commentControl_edit.setAttribute('src', '/images/commentEdit.png');
        commentControl_edit.style.float = 'right';
        commentControl_edit.style.marginTop = '3px';
        commentControl_edit.style.cursor = 'pointer';
        commentControl_edit.addEventListener('click', editComment);

        commentControl.appendChild(commentControl_edit);
    } else {
        var commentControl_report = document.createElement('img');
        commentControl_report.classList.add('commentReport');
        commentControl_report.setAttribute('src', '/images/commentReport.png');
        commentControl_report.style.float = 'right';
        commentControl_report.style.marginTop = '4px';
        commentControl_report.style.marginLeft = '12px';
        commentControl_report.style.cursor = 'pointer';
        commentControl_report.addEventListener('click', createCommentReportDiv);

        commentControl.appendChild(commentControl_report);
    }

    commentContentField.appendChild(commentContent);
    commentContentField.appendChild(commentControl);

    commentCard.appendChild(commentInfo);
    commentCard.appendChild(commentContentField);

    commentFrag.appendChild(commentCard);

    commentList.appendChild(commentFrag);

    setCommentCardWidth();
    changeCommentNicknameMargin();
};

var deleteComment = function (e) {
    showPopup(1, 'Really?', function (returnVal) {
        if (returnVal) {
            var comment_x = e.target;
            var commentId = comment_x.parentNode.parentNode.parentNode.childNodes[0].childNodes[2].innerText;
            var commentCard = comment_x.parentNode.parentNode.parentNode;

            var deleteCommentData = {
                commentId: commentId
            };

            $.ajax({
                type: "POST",
                async: true,
                url: '/artwork/comment/delete',
                data: deleteCommentData,
                success: function (data) {
                    commentCard.parentNode.removeChild(commentCard);
                }
            });
        }
    });
};

var editComment = function (e) {
    var commentField = e.target.parentNode.parentNode;
    var commentText = commentField.childNodes[0].innerText;

    commentField.removeChild(commentField.childNodes[0]);
    commentField.removeChild(commentField.childNodes[0]);

    commentField.style.backgroundColor = '#f9f9f8';
    commentField.style.height = '20px';

    var commentInputFrag = document.createDocumentFragment();

    var commentInputField = document.createElement('input');
    commentInputField.classList.add('commentEditContent');
    commentInputField.style.backgroundColor = '#f9f9f8';
    commentInputField.style.border = 'transparent';
    commentInputField.style.float = 'left';
    commentInputField.style.width = '550px';
    commentInputField.style.height = '20px';
    commentInputField.style.lineHeight = '20px';
    commentInputField.style.color = '#2d2d2c';
    commentInputField.style.fontFamily = 'Georgia, Noto Serif';
    commentInputField.style.fontSize = '15px';
    commentInputField.style.fontStyle = 'italic';
    commentInputField.style.fontWeight = '500';
    commentInputField.value = commentText;

    commentInputFrag.appendChild(commentInputField);

    var commentEditBtn = document.createElement('div');
    commentEditBtn.classList.add('commentEditBtn');
    commentEditBtn.style.float = 'left';
    commentEditBtn.style.width = '30px';
    commentEditBtn.style.height = '23px';
    commentEditBtn.style.marginLeft = '13px';
    commentEditBtn.style.fontFamily = 'Georgia, Noto Serif';
    commentEditBtn.style.fontStyle = 'italic';
    commentEditBtn.style.fontSize = '15px';
    commentEditBtn.style.fontWeight = '500';
    commentEditBtn.style.color = '#2d2d2c';
    commentEditBtn.style.lineHeight = '23px';
    commentEditBtn.style.cursor = 'pointer';
    commentEditBtn.innerText = 'Edit';
    commentEditBtn.addEventListener('click', sendEditComment);

    commentInputFrag.appendChild(commentEditBtn);

    commentField.appendChild(commentInputFrag);

    $('#comment .commentCard .commentEditContent').keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            sendEditComment(event);
        }
    });
};

var sendEditComment = function (e) {
    var commentEditData = {
        commentId: e.target.parentNode.previousSibling.childNodes[2].innerText,
        content: e.target.parentNode.childNodes[0].value
    };

    $.ajax({
        type: "POST",
        async: true,
        url: '/artwork/comment/edit',
        data: commentEditData,
        success: function () {
            restoreEditComment(e, commentEditData);
        }
    });
};

var restoreEditComment = function (e, data) {
    var commentField = e.target.parentNode;

    commentField.removeChild(commentField.childNodes[0]);
    commentField.removeChild(commentField.childNodes[0]);

    commentField.style.backgroundColor = '#2d2d2c';

    var commentContentFrag = document.createDocumentFragment();

    var commentContent = document.createElement('div');
    commentContent.classList.add('commentContent');
    commentContent.style.float = 'left';
    commentContent.style.width = '527px';
    commentContent.style.wordBreak = 'break-all';
    commentContent.style.lineHeight = '20px';
    commentContent.innerText = data.content;

    var commentControl = document.createElement('div');
    commentControl.classList.add('commentControl');

    var commentControl_x = document.createElement('img');
    commentControl_x.classList.add('x');
    commentControl_x.setAttribute('src', '/images/whitex.png');
    commentControl_x.style.float = 'right';
    commentControl_x.style.margin = '4px 0 0 12px';
    commentControl_x.style.cursor = 'pointer';
    commentControl_x.addEventListener('click', deleteComment);

    commentControl.appendChild(commentControl_x);

    var commentControl_edit = document.createElement('img');
    commentControl_edit.classList.add('commentEdit');
    commentControl_edit.setAttribute('src', '/images/commentEdit.png');
    commentControl_edit.style.float = 'right';
    commentControl_edit.style.marginTop = '3px';
    commentControl_edit.style.cursor = 'pointer';
    commentControl_edit.addEventListener('click', editComment);

    commentControl.appendChild(commentControl_edit);


    commentContentFrag.appendChild(commentContent);
    commentContentFrag.appendChild(commentControl);

    commentField.appendChild(commentContentFrag);
};

var sendCommentReport = function (e) {
    var commentId = document.getElementById('report_box').previousSibling.childNodes[0].childNodes[2].innerText;
    var commentReportContent = $('#commentReportInput').val();

    showPopup(1, 'Really?', function (returnVal) {
        if (returnVal) {
            var deleteCommentData = {
                commentId: commentId,
                reportContent: commentReportContent
            };

            $.ajax({
                type: "POST",
                async: true,
                url: '/artwork/comment/report',
                data: deleteCommentData,
                success: function (data) {
                    showPopup(0, 'Report Complete');
                    restoreCommentReportDiv(e)
                }
            });
        }
    });
};

var createCommentReportDiv = function (e) {
    var commentCard = e.target.parentNode.parentNode.parentNode.nextSibling;
    var commentList = document.getElementById('commentList');
    var commentSpacer = document.getElementById('commentSpacer');
    var commentReportDiv = document.getElementById('report_box').cloneNode(true);

    commentSpacer.removeChild(document.getElementById('report_box'));

    commentReportDiv.style.display = 'block';

    commentList.insertBefore(commentReportDiv, commentCard);

    $('#comment .commentControl .commentReport').unbind('click');
    e.target.addEventListener('click', restoreCommentReportDiv);

    $('#commentReportBtn').click(function (e) {
        if ($('#report_box').css('display') === 'block') {
            sendCommentReport(e);
        }
    });

    $('#commentReportInput').keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            sendCommentReport(e);
        }
    });
};

var restoreCommentReportDiv = function (e) {
    var commentList = document.getElementById('commentList');
    var commentSpacer = document.getElementById('commentSpacer');
    var commentReportDiv = document.getElementById('report_box').cloneNode(true);
    commentList.removeChild(document.getElementById('report_box'));

    commentReportDiv.style.display = 'none';
    commentReportDiv.childNodes[0].value = '';

    commentSpacer.appendChild(commentReportDiv);

    e.target.removeEventListener('click', restoreCommentReportDiv);
    $('#comment .commentControl .commentReport').unbind('click');

    $('#comment .commentControl .commentReport').click(function (e) {
        createCommentReportDiv(e);
    });

    $('#commentReportBtn').unbind('click');
};

var subscribe = function () {
    var sendData = {
        user_id: user.get(),
        artist_id: document.getElementById('userId').innerText
    };

    $.ajax({
        type: "POST",
        async: true,
        url: '/user/subscribe',
        data: sendData,
        success: function (data) {
            var subscribeBtn = document.getElementById('userInfo').childNodes[3];
            subscribeBtn.setAttribute('id', 'subscribed');
            subscribeBtn.childNodes[0].setAttribute('src', '/images/master_subscribed.png');
            $(subscribeBtn).unbind('click');
            $(subscribeBtn).click(function () {
                unSubscribe();
            });
        }
    });
};

var unSubscribe = function () {
    var sendData = {
        user_id: user.get(),
        artist_id: document.getElementById('userId').innerText
    };

    $.ajax({
        type: "POST",
        async: true,
        url: '/user/unSubscribe',
        data: sendData,
        success: function (data) {
            var unSubscribeBtn = document.getElementById('userInfo').childNodes[3];
            unSubscribeBtn.setAttribute('id', 'subscribe');
            unSubscribeBtn.childNodes[0].setAttribute('src', '/images/master_subscribe.png');
            $(unSubscribeBtn).unbind('click');
            $(unSubscribeBtn).click(function () {
                subscribe();
            });
        }
    });
};

var fullscreenDiv = $("#fullScreen");
fullscreenDiv.css({
    'position': 'fixed',
    'left': '0px',
    'top': '0px',
    'display': 'none',
    'box-sizing': 'border-box',
    'width': '100%',
    'height': '100%',
    'background': '#2d2d2c',
    'line-height': window.innerHeight + "px",
    'text-align': 'center'
});

var showFullScreen = function (e) {
    var image = {
        id: document.getElementById('artworkId').innerText
    };

    $.get('http://54.64.254.152/?user_id=' + user.get()
    + '&item_id=' + image.id + '&status=1', function () {
    });

    $(window).on("mousewheel.disableScroll DOMMouseScroll.disableScroll touchmove.disableScroll", function (e) {
        e.preventDefault();
    });

    var originalLikeBtn = e.parentNode.parentNode.childNodes[1].childNodes[0].childNodes[0];
    if ($(originalLikeBtn).attr('src') === '/images/like.png') {
        $('#fullScreen #likeButton img').attr('src', '/images/full_like.png');
        $('#fullScreen #likeButton').click(function () {
            $.get('http://54.64.254.152/?user_id=' + user.get()
            + '&item_id=' + image.id + '&status=2', function () {
            }).done(function () {
                console.log('like complete')
            });

            $('#fullScreen #likeButton img').attr('src', '/images/full_liked.png');

            var likeButton = {};
            likeButton.target = e.parentNode.parentNode.childNodes[1].childNodes[0].childNodes[0];
            likeButtonChange(likeButton, false);
        });
    } else {
        $('#fullScreen #likeButton img').attr('src', '/images/full_liked.png');
        $('#fullScreen #likeButton').click(function () {
            $.get('http://54.64.254.152/?user_id=' + user.get()
            + '&item_id=' + image.id + '&status=-1', function () {
            }).done(function () {
                console.log('unlike complete')
            });

            $('#fullScreen #likeButton img').attr('src', '/images/full_like.png');

            var likeButton = {};
            likeButton.target = e.parentNode.parentNode.childNodes[1].childNodes[0].childNodes[0];
            likedButtonChange(likeButton, false);
        });
    }

    var originalSubBtn = document.getElementById('userInfo').childNodes[3].childNodes[0];
    $('#fullScreen #subButton').unbind('click');
    if ($(originalSubBtn).attr('src') === '/images/master_subscribe.png') {
        $('#fullScreen #subButton img').attr('src', '/images/master_full_subscribe.png');
        $('#fullScreen #subButton').click(function () {
            subscribe();

            $('#fullScreen #subButton img').attr('src', '/images/master_full_subscribed.png');

            originalSubBtn.parentNode.setAttribute('id', 'subscribed');
            originalSubBtn.setAttribute('src', '/images/master_subscribed.png');
        });
    } else {
        $('#fullScreen #subButton img').attr('src', '/images/master_full_subscribed.png');
        $('#fullScreen #subButton').click(function () {
            unSubscribe();

            $('#fullScreen #subButton img').attr('src', '/images/master_full_subscribe.png');

            originalSubBtn.parentNode.setAttribute('id', 'subscribe');
            originalSubBtn.setAttribute('src', '/images/master_subscribe.png');
        });
    }

    var cloneImage = e.cloneNode(true);
    cloneImage.setAttribute('id', 'fullArtwork');
    cloneImage.removeAttribute('style');

    var fullscreen = document.getElementById('fullScreen');
    fullscreen.appendChild(cloneImage);

    var windowRatio = (window.innerWidth) / (window.innerHeight);
    var imageRatio = parseFloat(e.offsetWidth) / parseFloat(e.offsetHeight);

    if (windowRatio > imageRatio) {
        $('#fullArtwork').removeAttr("style");
        $('#fullArtwork').css('height', window.innerHeight);
        $('#fullArtwork').css('width', window.innerHeight * imageRatio);
        $('#fullArtwork').css('position', 'fixed');
        $('#fullArtwork').css('top', '0');
        $('#fullArtwork').css('left', '50%');
        $('#fullArtwork').css('margin-left', parseFloat($('#fullArtwork').css('width')) * (-0.5));
    } else if (windowRatio < imageRatio) {
        $('#fullArtwork').removeAttr("style");
        $('#fullArtwork').css('width', window.innerWidth);
        $('#fullArtwork').css('height', window.innerWidth / imageRatio);
        $('#fullArtwork').css('position', 'fixed');
        $('#fullArtwork').css('top', '50%');
        $('#fullArtwork').css('left', '0');
        $('#fullArtwork').css('margin-top', parseFloat($('#fullArtwork').css('height')) * (-0.5));
    } else {
        $('#fullArtwork').removeAttr("style");
        $('#fullArtwork').css('top', '0');
        $('#fullArtwork').css('left', '0');
        $('#fullArtwork').css('height', window.innerHeight);
        $('#fullArtwork').css('width', window.innerWidth);
    }

    $(window).resize(function () {
        var windowRatio = (window.innerWidth) / (window.innerHeight);

        if (windowRatio > imageRatio) {
            $('#fullArtwork').removeAttr("style");
            $('#fullArtwork').css('height', window.innerHeight);
            $('#fullArtwork').css('width', window.innerHeight * imageRatio);
            $('#fullArtwork').css('position', 'fixed');
            $('#fullArtwork').css('top', '0');
            $('#fullArtwork').css('left', '50%');
            $('#fullArtwork').css('margin-left', parseFloat($('#fullArtwork').css('width')) * (-0.5));
        } else if (windowRatio < imageRatio) {
            $('#fullArtwork').removeAttr("style");
            $('#fullArtwork').css('width', window.innerWidth);
            $('#fullArtwork').css('height', window.innerWidth / imageRatio);
            $('#fullArtwork').css('position', 'fixed');
            $('#fullArtwork').css('top', '50%');
            $('#fullArtwork').css('left', '0');
            $('#fullArtwork').css('margin-top', parseFloat($('#fullArtwork').css('height')) * (-0.5));
        } else {
            $('#fullArtwork').removeAttr("style");
            $('#fullArtwork').css('top', '0');
            $('#fullArtwork').css('left', '0');
            $('#fullArtwork').css('height', window.innerHeight);
            $('#fullArtwork').css('width', window.innerWidth);
        }
    });

    $('#fullScreen').click(function () {
        closeFullScreen();
    });

    $('#fullScreen #likeButton').click(function (e) {
        e.stopPropagation();
    });

    $('#fullScreen #subButton').click(function (e) {
        e.stopPropagation();
    });

    $('img').bind('contextmenu', function () {
        return false;
    });

    fullscreenDiv.css('display', 'block');
};

var closeFullScreen = function () {
    var fullscreen = document.getElementById('fullScreen');
    var fullImage = fullscreen.childNodes[2];

    fullscreen.removeChild(fullImage);

    $('#fullScreen #likeButton img').attr('src', '/images/full_like.png');
    $('#fullScreen #likeButton img').attr('src', '/images/master_full_subscribe.png');

    $('#fullScreen #likeButton').unbind('click');
    $('#fullScreen #subButton').unbind('click');
    $('#fullScreen').unbind('click');
    $(window).unbind('resize');

    fullscreenDiv.css('display', 'none');

    $(window).off("mousewheel.disableScroll DOMMouseScroll.disableScroll touchmove.disableScroll");
};

$(document).ready(function () {
    $.get('/user/getUserId', function (data) {
    }).done(function (data) {
        user.set(data.id);
    });

    $('#artworkImage').click(function (e) {
        showFullScreen(e.target);
    });

    $('#liked #likedIcon img').click(function (e) {
        likedButtonChange(e);
    });

    $('#like #likeIcon img').click(function (e) {
        likeButtonChange(e);
    });

    $('#commentForm #commentEnter').click(function (e) {
        sendComment();
    });

    $('#commentForm #commentInput').keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            sendComment();
        }
    });

    $('#comment .commentControl .x').click(function (e) {
        deleteComment(e);
    });

    $('#comment .commentControl .commentReport').click(function (e) {
        createCommentReportDiv(e);
    });


    $('#comment .commentControl .commentEdit').click(function (e) {
        editComment(e);
    });

    $('#subscribe').click(function () {
        subscribe();
    });

    $('#subscribed').click(function () {
        unSubscribe();
    });

    $('#share').click(function () {
        $('#shareBox').css('display', 'block');
    });

    //use zeroClipboard lib to click-copy
    var clip = new ZeroClipboard($("#share_url"));

    clip.setText(document.URL);

    clip.on("ready", function () {
        this.on("aftercopy", function (event) {
            showPopup(0, 'Successfully Copied to your clipboard!');
        });
    });

    clip.on("error", function (event) {
        showPopup(0, 'error occurred, please try again');
    });

    $('#shareBox #share_x').click(function () {
        $('#shareBox').css('display', 'none');
    });

    setCommentCardWidth();
    changeCommentNicknameMargin();
});