/**
 * Created by youngkim on 2014. 11. 12..
 */

'use strict';

var validForm = (function () {
    var mailField = document.getElementById('email'),
        psField = document.getElementById('password'),
        psReField = document.getElementById('rePassword');

    var emailRegExp = /.+\@.+\..+/;

    var validEmailFormat = function () {
        return (emailRegExp.test(mailField.value));
    };

    var validPassword = function () {
        return (psReField.value === psField.value);
    };

    var resetPsField = function () {
        psField.type = "password";
        psField.value = "";
        psReField.type = "password";
        psReField.value = "";
    };

    return {
        validEmailFormat: validEmailFormat,
        validPassword: validPassword,
        resetPassword: resetPsField
    };
}());

var submitForm = (function () {
    var sendRequest = function () {
        if (validForm.validPassword() && validForm.validEmailFormat()) {
            document.forms['join-form'].submit();
        }
        else {
            validForm.resetPassword();
        }
    };

    return {
        sendRequest: sendRequest
    }
})();

var submit = submitForm.sendRequest;

$( "#email" ).keypress(function( event ) {
    if ( event.which == 13 ) {
        event.preventDefault();
        submit();
    }
});

$( "#password" ).keypress(function( event ) {
    if ( event.which == 13 ) {
        event.preventDefault();
        submit();
    }
});

$( "#rePassword" ).keypress(function( event ) {
    if ( event.which == 13 ) {
        event.preventDefault();
        submit();
    }
});