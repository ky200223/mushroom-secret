/**
 * Created by youngkim on 2014. 11. 15..
 */

'use strict';

(function () {
    document.getElementById("introduction").value =
        document.getElementById("introductionText").innerText;
})();

var loadingDiv = $("#loadingDiv");
{
    loadingDiv.css({
        'position': 'fixed',
        'left': '0px',
        'top': '0px',
        'display': 'none',
        'box-sizing': 'border-box',
        'width': '100%',
        'height': '100%',
        'background': 'rgba(45, 45, 44, 0.80)',
        'color': '#fff',
        'line-height': window.innerHeight + "px",
        'text-align': 'center'
    });
    loadingDiv.html("Loading...");
}
function uploadFile(dataName, fileData, inputId) {
    var formData = new FormData();
    formData.append(dataName, fileData);

    $.ajax({
        type: "POST",
        async: true,
        url: "http://image.mushroom.land/images/upload",
        contentType: false,
        processData: false,
        data: formData,
        beforeSend: function () {
            $("#" + inputId).val("");
        },
        success: function (data) {
            $("#" + inputId).val(data);
        },
        fail: function () {
            if (confirm(inputId + " upload is fail! Retry?") == true)
                uploadFile(dataName, fileData, inputId);
        }
    });
}

var validForm = (function () {
    var psField = document.getElementById('password'),
        psNewField = document.getElementById('newPassword'),
        psReField = document.getElementById('rePassword');

    var enterPassword = function () {
        return (psField.value != '');
    };

    var validPassword = function () {
        if (psNewField != '') {
            return (psReField.value === psNewField.value);
        }
        return true;
    };

    var resetPsField = function () {
        psNewField.type = "password";
        psNewField.value = "";
        psReField.type = "password";
        psReField.value = "";
    };

    return {
        enterPassword: enterPassword,
        validPassword: validPassword,
        resetPassword: resetPsField
    };
}());
function sendForm() {
    loadingDiv.css("display", "block");
    $("body").css("overflow", "hidden");
    $(window).on("mousewheel.disableScroll DOMMouseScroll.disableScroll touchmove.disableScroll", function (e) {
        e.preventDefault();
    });

    if ($("#profilePic").val() != "" && $("#profilePicStr").val() == "") {
        loadingDiv.html("Please wait your profile image uploading is done.");
        setTimeout(sendForm, 500);
        return false;
    }

    $("form[name='profile-form']").submit();
    $(window).off(".disableScroll");
    return true;
}
var submitForm = (function () {
    var sendRequest = function () {
        var nick = $("#nickName");
        if (nick.val().replace(/ /g,'').length > 16) {
            showPopup(0, "Nickname has to be less than 16 letters");
            nick.val(nick.val().substr(0, 16));
            nick.focus();
            return false;
        }
        if (validForm.validPassword() && validForm.enterPassword()) {
            sendForm();
        } else if (document.getElementById('newPassword').value === '') {
            sendForm();
        } else {
            validForm.resetPassword();
            showPopup(0, 'Please type PW again!');
        }
    };

    return {
        sendRequest: sendRequest
    }
})();

var submit = submitForm.sendRequest;

//real time image thumbnail
var imageValidate = function (dataURL, blob) {
    if (!blob.size) return;

    if (blob.size > 10485760) { // less than 10 MB
        alert("File size is too big!");
        return;
    }

    if (/^image/.test(blob.type)) { // only image file
        blob = new File([blob], "blob.png", {type: blob.type})
        uploadFile("image", blob, "profilePicStr");

        $("#profileImage").css("background-image", "url(" + dataURL + ")");
        $("#profileImage").css("background-position", "center center");
        $("#profileImage").css("background-size", "cover");
    }
};

var options = {
    thumbBox: '.thumbBox',
    spinner: '.spinner',
    imgSrc: ''
};
var cropper = $('#cropImageBox').cropbox(options);

var imageCrop = function () {
    if (this.files.length == 0)
        return;

    var reader = new FileReader();
    reader.onload = function (e) {
        options.imgSrc = e.target.result;
        cropper = $('#cropImageBox').cropbox(options);
    };
    reader.readAsDataURL(this.files[0]);
    //this.value = '';

    $("#profileImage").css('display', 'none');
    $("#cropBox").css('display', 'block');
};

$(function () {
    $("#profilePic").on("change", imageCrop);
    $("#profileImage").click(function () {
        $("#profilePic").click();
    });

    $("#cropImageBtn").on("click", function () {
        var img = cropper.getDataURL();
        var blob = cropper.getBlob();

        imageValidate(img, blob);

        $("#cropBox").css('display', 'none');
        $("#profileImage").css('display', 'block');
    });
});


var addrV = document.createElement('div');
{
    addrV.style.float = "right";
    addrV.style.backgroundImage = "url(/images/addressArrow.png)";
    addrV.style.width = "11px";
    addrV.style.height = "7px";
    addrV.style.margin = "6 20 0 -40";
}
var addrA = document.createElement('div');
{
    addrA.style.float = "right";
    addrA.style.backgroundImage = "url(/images/uparrow.png)";
    addrA.style.width = "11px";
    addrA.style.height = "7px";
    addrA.style.margin = "6 20 0 -40";
}

var AddrSelect = function () {
    var address = document.getElementById("address");
    address.value = this.firstChild.innerHTML;

    var list = this.parentNode;
    for (var i = 0; i < list.childElementCount; ++i) {
        list.children[i].style.display = "none";
        list.children[i].onclick = null;
    }
    this.style.display = "block";
    this.style.borderTop = "1px solid #2d2d2c";
    this.style.borderBottom = "1px solid #2d2d2c";
    this.onclick = AddrModify;

    list.firstChild.removeChild(list.firstChild.childNodes[1]);
    this.appendChild(addrV);
};

var AddrModify = function () {
    var list = this.parentNode;
    this.removeChild(this.childNodes[1]);
    list.firstChild.appendChild(addrA);
    for (var i = 0; i < list.childElementCount; ++i) {
        list.children[i].style.display = "block";
        list.children[i].style.borderBottom = "transparent";
        list.children[i].style.borderTop = "transparent";
        if (i === 0) {
            list.children[i].style.borderTop = "1px solid #2d2d2c";
        }
        if (i === list.childElementCount - 1) {
            list.children[i].style.borderBottom = "1px solid #2d2d2c";
        }
        list.children[i].onclick = AddrSelect;
    }
};

// initialize address list
var addrList = document.getElementById('addrList');
var address = document.getElementById("address");
for (var i = 0; i < addrList.childElementCount; ++i) {
    if (addrList.children[i].firstChild.innerHTML == address.value) {
        addrList.children[i].style.display = "block";
        addrList.children[i].onclick = AddrModify;
        addrList.children[i].appendChild(addrV);
    }
}

var nickNameCheck = function () {
    if ($(this).val().replace(/ /g,'').length > 16) {
        showPopup(0, "Nickname has to be less than 16 letters");
        $(this).val($(this).val().substr(0, 16));
        $(this).focus();
        return false;
    }
    return true;
};

$(function () {
    $("#nickName").change(nickNameCheck);
    $("#nickName").keypress(nickNameCheck);
});

/*****************************
 * Google plus link
 *
 * 1. get google access token
 * 2. get user's data
 * 3. send data
 *****************************/
var googlePlus = false;

$('#googlePlusBtn').click(function () {
    googlePlus = true;
});

(function () {
    var po = document.createElement('script');
    po.type = 'text/javascript';
    po.async = true;
    po.src = 'https://apis.google.com/js/client:plusone.js?onload=render';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(po, s);
})();

function render() {
    gapi.signin.render('googlePlusBtn', {
        'callback': 'googlePlusSignInCallback',
        'clientid': '264201535369-qg1tbsucfkjujbr4un0ueki40b45hsq7.apps.googleusercontent.com',
        'cookiepolicy': 'single_host_origin',
        'requestvisibleactions': 'http://schemas.google.com/AddActivity',
        'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
    });
}

// 1. get google access token
function googlePlusSignInCallback(authResult) {
    // authentication success
    if (authResult['access_token']) {
        gapi.auth.setToken(authResult);     // store token

        // 2. get Data
        getEmailFromGoogleAPI(function (userData) {
            if (googlePlus) {
                // 3. send token
                $.ajax({
                    type: 'POST',
                    url: '/user/link/google',
                    contentType: 'application/json',
                    success: function (result) {
                        window.location.reload();
                    },
                    error: function (request, status, error) {
                        console.error('Ajax error: googlePlusLink function.');
                    },
                    processData: false,
                    data: JSON.stringify({
                        userData: userData
                    })
                });
            }
        });
    }
    // authentication fail
    else if (authResult['error']) {
        if (authResult['error'] == 'access_denied')
            console.log('사용자가 Google plus login 을 한다고 했다가 창이 뜨니깐 동의 하지 않았습니다.');
        else if (authResult['error'] == 'immediate_failed')
            console.log('사용자가 Google plus에 자동으로 로그인 할 수 없습니다.');
    }

    function getEmailFromGoogleAPI(callback) {
        gapi.client.load('oauth2', 'v2', function () {
            var request = gapi.client.oauth2.userinfo.get();
            request.execute(getEmailCallback);
        });

        function getEmailCallback(obj) {
            callback(obj);
        }
    }
}

/*****************************
 * Facebook login
 *
 * 1. get facebook module
 * 2. get user's data
 * 3. send data
 *****************************/
var facebook = false;

$('#facebookBtn').click(function () {
    login();
});

// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        facebook = true;
    } else if (response.status === 'not_authorized') {
        // The person is logged into Facebook, but not your app.
        console.log('사용자가 facebook Login 을 한다고 했다가 창이 뜨니깐 동의 하지 않았습니다.');
    } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
        console.log('사용자가 facebook에 자동으로 로그인 할 수 없습니다.');
    }
}

function checkLoginState() {
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function () {
    FB.init({
        appId: '606556316139036',
        xfbml: true,
        cookie: true,
        version: 'v2.2'
    });

    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function login() {
    if (facebook) {
        FB.api('/me', function (response) {
            $.ajax({
                type: 'POST',
                url: '/user/link/facebook',
                contentType: 'application/json',
                success: function (result) {
                    window.location.reload();
                },
                error: function (request, status, error) {
                    console.error('Ajax error: facebookLogin function.');
                },
                processData: false,
                data: JSON.stringify({
                    userData: response
                })
            });
        });
    } else {
        showPopup(0, 'please login to facebook to link your facebook info to mushroom');
    }
}