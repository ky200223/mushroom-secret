/**
 * Created by youngkim on 2014. 11. 18..
 */

function footerRelocate() {
    var footer = document.getElementsByTagName('footer')[0];
    var dummy = document.getElementById('footerDummy');
    dummy.style.height = '0px';
    var footerPos = getAbsoluteXY(footer);
    var dummyHeight = document.body.clientHeight - footerPos.height + 60;

    if (dummyHeight > footerPos.top) {
        dummyHeight = dummyHeight - footerPos.top;
        dummy.style.height = dummyHeight;
    }
}
$(window).scroll(function () {
    footerRelocate();
});
$(window).resize(function () {
    footerRelocate();
});
footerRelocate();