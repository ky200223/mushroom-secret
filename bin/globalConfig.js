/**
 * Created by youngkim on 2014. 11. 4..
 */

var path = require('path'),
    debug = require('debug');

if (process.env.SERVER_SETTING === 'undefined' || process.env.SERVER_SETTING === undefined) {
    throw new Error('export SERVER_SETTING (local or development, production) required.');
}

SETTING = {};

PROJECT_ROOT = path.dirname(__dirname);
SOURCE_ROOT = path.join(PROJECT_ROOT, 'src');
//TEST_ROOT = path.join(PROJECT_ROOT, 'test');

info = debug('info:');
info.log = console.log.bind(console);
error = debug('error:');
error.log = console.error.bind(console);

(function () {
    var env = process.env.SERVER_SETTING;

    var local = {
        flag: 'local',
        ports: [8001, 8002, 8003, 8004],
        mysqlConfig: {
            host: 'localhost',
            port: 3306,
            user: 'mushroom_admin',
            password: 'ajtnlfna1234',
            database: 'mushroom'
        },
        mailConfig: {
            service: 'Gmail',
            auth: {
                user: 'info@mushroom.land',
                pass: 'ajtnlfna14123'
            }
        },
        url : "localhost:3000",
        recommendEngine: 'http://services.snu.ac.kr'
    };

    var development = {
        flag: 'development',
        ports: [8001, 8002, 8003, 8004],
        mysqlConfig: {
            host: '54.65.58.43',
            port: 3306,
            user: 'mushroom_admin',
            password: 'ajtnlfna1234',
            database: 'mushroom'
        },
        mailConfig: {
            service: "Gmail",
            auth: {
                user: "info@mushroom.land",
                pass: "ajtnlfna14123"
            }
        },
        url : "localhost:3000",
        recommendEngine: 'http://services.snu.ac.kr'
    };

    var production = {
        flag: 'production',
        ports: [8001, 8002, 8003, 8004],
        mysqlConfig: {
            host: '54.65.58.43',
            port: 3306,
            user: 'mushroom_admin',
            password: 'ajtnlfna1234',
            database: 'mushroom'
        },
        mailConfig: {
            service: "Gmail",
            auth: {
                user: "info@mushroom.land",
                pass: "ajtnlfna14123"
            }
        },
        url : "http://mushroom.land",
        recommendEngine: 'http://services.snu.ac.kr'
    };

    switch (env) {
        case 'local' :
            SETTING = local;
            break;
        case 'development' :
            SETTING = development;
            break;
        case 'production' :
            SETTING = production;
            break;
        default :
            throw new Error('export SERVER_SETTING (local or development, production) required.');
    }
})();

module.exports = SETTING;