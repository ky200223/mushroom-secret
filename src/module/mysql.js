/**
 * Created by youngkim on 2014. 11. 6..
 */

'use strict';

var extractConnection = (function () {
    var mysql = require('mysql');
    var conn;

    var getConnection = function () {
        conn = mysql.createConnection(SETTING.mysqlConfig);
        return conn;
    };

    return {
        getConnection: getConnection
    };
}());

module.exports = extractConnection;