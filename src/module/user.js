/**
 * Created by youngkim on 2014. 11. 17..
 */

'use strict';

var mysqlConn = require(SOURCE_ROOT + '/module/mysql').getConnection();
var crypto = require('crypto');

var loginGooglePlusUser = function (req, res, userData) {
    var userEmail = userData.email.split('@')[0] + userData.id.toString().slice(17) + '@mushroom.land';

    req.session.isAdmin = false;
    req.session.loginStatus = true;
    req.session.useremail = userEmail;

    res.clearCookie('googleplus');
    res.cookie('googleplus', 'true', { maxAge: 900000, httpOnly: true});

    mysqlConn.query(
        'SELECT nickname FROM user WHERE email = ? AND googleplus_login = 1', [userEmail], function (err, result) {
            if (err) error('user module - loginGooglePlusUser err : ' + err);
            if (result.length === 0) {
                req.session.nickname = userData.name;
                res.redirect('/recommend');
            } else {
                req.session.nickname = result[0].nickname;
                res.redirect('/recommend');
            }
        }
    );
};

var registerGooglePlusUser = function (req, res, userData) {
    var userEmail = userData.email.split('@')[0] + userData.id.toString().slice(17) + '@mushroom.land';

    var data = {
        email: userEmail,
        nickname: userData.name + crypto.createHash('sha512').update(userEmail + 'google').digest('hex').slice(0, 3),
        password: crypto.createHash('sha512').update('google').digest('hex'),
        profile_url: '0',
        homepage: '',
        facebook: '',
        facebook_login: 0,
        googleplus: userData.link,
        googleplus_login: 1,
        address: 'Address',
        gallery: '',
        introduction: '',
        join_time: new Date(new Date().toJSON()),
        user_type: '1'
    };

    mysqlConn.query(
        'INSERT INTO user SET ?', data, function (err) {
            if (err) error('user module - registerGooglePlusUser err : ' + err);
            loginGooglePlusUser(req, res, userData);
        }
    );
};

exports.handleGooglePlusUser = function (req, res, userData) {
    var userEmail = userData.email.split('@')[0] + userData.id.toString().slice(17) + '@mushroom.land';

    mysqlConn.query(
        'SELECT id FROM user WHERE email = ? AND googleplus_login = 1', [userEmail], function (err, result) {
            if (err) {
                error('user module - existsUserByGooglePlus error');
            }
            if (result.length != 0) {
                loginGooglePlusUser(req, res, userData);
            } else {
                registerGooglePlusUser(req, res, userData);
            }
        }
    );
};

var loginFacebookUser = function (req, res, userData) {
    var userEmail = userData.first_name + userData.last_name + userData.id.toString().slice(11) + '@mushroom.land';

    req.session.isAdmin = false;
    req.session.loginStatus = true;
    req.session.useremail = userEmail;

    res.clearCookie('facebook');
    res.cookie('facebook', 'true', { maxAge: 900000, httpOnly: true});

    mysqlConn.query(
        'SELECT nickname FROM user WHERE email = ? AND facebook_login = 1', [userEmail], function (err, result) {
            if (err) error('user module - loginFacebookUser err : ' + err);
            if (result.length === 0) {
                req.session.nickname = userData.name;
                res.redirect('/recommend');
            } else {
                req.session.nickname = result[0].nickname;
                res.redirect('/recommend');
            }
        }
    );
};

var registerFacebookUser = function (req, res, userData) {
    var userEmail = userData.first_name + userData.last_name + userData.id.toString().slice(11) + '@mushroom.land';

    var data = {
        email: userEmail,
        nickname: userData.name + crypto.createHash('sha512').update(userEmail + 'facebook').digest('hex').slice(0, 3),
        password: crypto.createHash('sha512').update('facebook').digest('hex'),
        profile_url: '0',
        homepage: '',
        facebook: 'http://www.facebook.com/' + userData.id,
        facebook_login: 1,
        googleplus: '',
        googleplus_login: 0,
        address: 'Address',
        gallery: '',
        introduction: '',
        join_time: new Date(new Date().toJSON()),
        user_type: '1'
    };

    mysqlConn.query(
        'INSERT INTO user SET ?', data, function (err) {
            if (err) error('user module - registerFacebookUser err : ' + err);
            loginFacebookUser(req, res, userData);
        }
    );
};

exports.handleFacebookUser = function (req, res, userData) {
    var userEmail = userData.first_name + userData.last_name + userData.id.toString().slice(11) + '@mushroom.land';

    mysqlConn.query(
        'SELECT id FROM user WHERE email = ? AND facebook_login = 1', [userEmail], function (err, result) {
            if (err) {
                error('user module - existsUserByFacebook error');
            }
            if (result.length != 0) {
                loginFacebookUser(req, res, userData);
            } else {
                registerFacebookUser(req, res, userData);
            }
        }
    );
};