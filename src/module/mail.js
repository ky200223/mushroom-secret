/**
 * Created by youngkim on 2014. 11. 13..
 */

'use strict';

var mail = (function () {
    var nodemailer = require("nodemailer");

    var mailConfig = {
        service: "Gmail",
        auth: {
            user: "",
            pass: ""
        }
    };

    var setInfoAccount = function () {
        mailConfig = SETTING.mailConfig;
    };

    var setMail = function (email) {
        mailConfig.auth.user = email;
    };

    var setPassword = function (password) {
        mailConfig.auth.pass = password;
    };

    var createConn = function () {
        if (mailConfig.auth.user === "" && mailConfig.auth.pass === "") {
            setInfoAccount();
        }
        return nodemailer.createTransport(mailConfig);
    };

    return {
        createConn: createConn,
        setInfoAccount: setInfoAccount,
        setMail: setMail,
        setPassword: setPassword
    }
}());

module.exports = mail;