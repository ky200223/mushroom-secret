/**
 * Created by youngkim on 2014. 11. 12..
 */

'use strict';

// route start from '/user'

var express = require('express'),
    router = express.Router();

var userHandler = require(SOURCE_ROOT + '/handler/userHandler');

router.use(function (req, res, next) {
    next();
});

/*
 * 유저가 로그인 되어 있는 것을 체크한다.
 */
var ensureUserLogin = function (req, res, next) {
    if (req.session.loginStatus === true && req.session.useremail != undefined && req.session.nickname != undefined) {
        return next();
    }
    req.session.loginStatus = false;
    req.session.useremail = undefined;
    req.session.nickname = undefined;
    res.redirect('/user/login');
};

/*
 * 유저가 로그아웃 되어 있는 것을 체크한다.
 */
var ensureUserLogout = function (req, res, next) {
    if (!req.session.loginStatus) {
        return next();
    }
    res.redirect('/recommend');
};

/*
 * 유저가 구글로 로그인 되어 있는 것을 체크한다.
 */
var ensureGoogleLogin = function (req, res, next) {
    if (req.cookies.googleplus === 'true') {
        return next();
    } else {
        res.status(200).send();
    }
};

/*
 * 유저가 페이스북으로 로그인 되어 있는 것을 체크한다.
 */
var ensureFacebookLogin = function (req, res, next) {
    if (req.cookies.facebook === 'true') {
        return next();
    } else {
        res.status(200).send();
    }
};

router.get('/login', ensureUserLogout, userHandler.viewLoginPage);
router.post('/login', ensureUserLogout, userHandler.handleLoginRequest);

router.post('/login/google', ensureUserLogout, ensureGoogleLogin, userHandler.loginGoogle);
router.post('/login/facebook', ensureUserLogout, ensureFacebookLogin, userHandler.loginFacebook);

router.get('/logout', userHandler.handleLogoutRequest);

//router.get('/join', function (req, res) {
//    res.render('error', {message: 'Coming soon!'})
//});
//router.get('/join2', ensureUserLogout, userHandler.viewJoinPage);

router.get('/join', ensureUserLogout, userHandler.viewJoinPage);
router.post('/join', ensureUserLogout, userHandler.handleJoinRequest);

router.get('/profile', ensureUserLogin, userHandler.viewProfilePage);
router.post('/profile', ensureUserLogin, userHandler.handleProfileRequest);

router.post('/link/google', ensureUserLogin, userHandler.linkGoogle);
router.post('/link/facebook', ensureUserLogin, userHandler.linkFacebook);

router.get('/mypage', ensureUserLogin, userHandler.viewCurUserPage);
router.get('/page/:artist_id', ensureUserLogin, userHandler.viewMasterPage);

router.get('/mailAuth/:authKey', ensureUserLogout, userHandler.userMailAuth);

router.get('/getUserId', ensureUserLogin, userHandler.getUserId);

router.post('/subscribe', ensureUserLogin, userHandler.handleSubscribeRequest);
router.post('/unSubscribe', ensureUserLogin, userHandler.handleUnSubscribeRequest);

module.exports = router;