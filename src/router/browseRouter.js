/**
 * Created by youngkim on 2014. 11. 12..
 */

'use strict';

// route start from '/browse'

var express = require('express'),
    router = express.Router();

var browseHandler = require(SOURCE_ROOT + '/handler/browseHandler');

router.use(function (req, res, next) {
    next();
});

/*
 * 유저가 로그인 되어 있는 것을 체크한다.
 */
var ensureUserLogin = function (req, res, next) {
    if (req.session.loginStatus && req.session.useremail != undefined && req.session.nickname != undefined) {
        return next();
    }
    req.session.loginStatus = false;
    req.session.useremail = undefined;
    req.session.nickname = undefined;
    res.redirect('/user/login');
};

router.get('/', ensureUserLogin, browseHandler.viewBrowsePage);

router.post('/artwork', ensureUserLogin, browseHandler.handleAllArtworkQueryRequest);
router.post('/artwork/:searchText', ensureUserLogin, browseHandler.handleArtworkQueryRequest);

router.post('/instant/artwork/:searchText', ensureUserLogin, browseHandler.handleInstantArtworkQueryRequest);

router.post('/artist', ensureUserLogin, browseHandler.handleAllArtistQueryRequest);
router.post('/artist/:searchText', ensureUserLogin, browseHandler.handleArtistQueryRequest);

router.post('/instant/artist/:searchText', ensureUserLogin, browseHandler.handleInstantArtistQueryRequest);

module.exports = router;