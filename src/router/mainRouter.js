/**
 * Created by youngkim on 2014. 11. 5..
 */

'use strict';

var recommendRouter = require(SOURCE_ROOT + '/router/recommendRouter'),
    browseRouter = require(SOURCE_ROOT + '/router/browseRouter'),
    userRouter = require(SOURCE_ROOT + '/router/userRouter'),
    uploadRouter = require(SOURCE_ROOT + '/router/uploadRouter'),
    flowRouter = require(SOURCE_ROOT + '/router/flowRouter'),
    artworkRouter = require(SOURCE_ROOT + '/router/artworkRouter');

/*
 * 라우팅을 시작한다.
 */
var router = function (app) {
    app.use('/recommend', recommendRouter);
    app.use('/browse', browseRouter);
    app.use('/user', userRouter);
    app.use('/upload', uploadRouter);
    app.use('/flow', flowRouter);
    app.use('/artwork', artworkRouter);
    app.use('/', function (req, res) {
        if (req.session.loginStatus) {
            res.redirect('/recommend');
        } else {
            res.render('landing');
        }
    });
};

module.exports = router;