/**
 * Created by youngkim on 2014. 11. 5..
 */

'use strict';

// route start from '/test'

var express = require('express'),
    router = express.Router();

router.use(function (req, res, next) {
    next();
});

router.get('/recommend', function (req, res) {
    res.render('recommend');
});

router.get('/join', function (req, res) {
    res.render('join');
});

router.get('/login', function (req, res) {
    res.render('login');
});

router.get('/profile', function (req, res) {
    res.render('profile');
    console.log('profile');
});

router.get('/browse', function (req, res) {
    res.render('browse');
});

router.get('/upload', function (req, res) {

});

router.get('/flow', function (req, res) {
    res.render('flow');
});

module.exports = router;