/**
 * Created by youngkim on 2014. 11. 12..
 */

'use strict';

// route start from '/flow'

var express = require('express'),
    router = express.Router();

var flowHandler = require(SOURCE_ROOT + '/handler/flowHandler');

router.use(function (req, res, next) {
    next();
});

/*
 * 유저가 로그인 되어 있는 것을 체크한다.
 */
var ensureUserLogin = function (req, res, next) {
    if (req.session.loginStatus && req.session.useremail != undefined && req.session.nickname != undefined) {
        return next();
    }
    req.session.loginStatus = false;
    req.session.useremail = undefined;
    req.session.nickname = undefined;
    res.redirect('/user/login');
};

router.get('/', ensureUserLogin, flowHandler.viewFlowPage);
router.get('/liked', ensureUserLogin, flowHandler.viewLikedPage);
router.get('/subscribed', ensureUserLogin, flowHandler.viewSubscribedPage);

router.post('/', ensureUserLogin, flowHandler.handleFlowQueryRequest);
router.post('/liked', ensureUserLogin, flowHandler.handleLikedQueryRequest);
router.post('/subscribed', ensureUserLogin, flowHandler.handleSubscribeQueryRequest);

module.exports = router;