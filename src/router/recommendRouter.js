/**
 * Created by youngkim on 2014. 11. 12..
 */

'use strict';

// route start from '/recommend'

var express = require('express'),
    router = express.Router();

var recommendHandler = require(SOURCE_ROOT + '/handler/recommendHandler');

router.use(function (req, res, next) {
    next();
});

/*
 * 유저가 로그인 되어 있는 것을 체크한다.
 */
var ensureUserLogin = function (req, res, next) {
    if (req.session.loginStatus && req.session.useremail != undefined && req.session.nickname != undefined) {
        return next();
    }
    req.session.loginStatus = false;
    req.session.useremail = undefined;
    req.session.nickname = undefined;
    res.redirect('/user/login');
};

router.get('/', ensureUserLogin, recommendHandler.viewRecommendPage);

router.post('/loadMore', ensureUserLogin, recommendHandler.handleLoadMoreRequest);
//router.get('/fullArtwork', ensureUserLogin, recommendHandler.viewFullArtworkPage);

module.exports = router;