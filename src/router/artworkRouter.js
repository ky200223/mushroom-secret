/**
 * Created by youngkim on 2014. 11. 21..
 */
'use strict';

// route start from '/user'

var express = require('express'),
    router = express.Router();

var artworkHandler = require(SOURCE_ROOT + '/handler/artworkHandler');

router.use(function (req, res, next) {
    next();
});

/*
 * 유저가 로그인 되어 있는 것을 체크한다.
 */
var ensureUserLogin = function (req, res, next) {
    if (req.session.loginStatus && req.session.useremail != undefined && req.session.nickname != undefined) {
        return next();
    }
    req.session.loginStatus = false;
    req.session.useremail = undefined;
    req.session.nickname = undefined;
    res.redirect('/user/login');
};

router.get('/get/:artwork_id', ensureUserLogin, artworkHandler.handleArtworkInfoRequest);
router.post('/comment/add', ensureUserLogin, artworkHandler.handleCommentAddRequest);
router.post('/comment/delete', ensureUserLogin, artworkHandler.handleCommentDeleteRequest);
router.post('/comment/report', ensureUserLogin, artworkHandler.handleCommentReportRequest);
router.post('/comment/edit', ensureUserLogin, artworkHandler.handleCommentEditRequest);
module.exports = router;