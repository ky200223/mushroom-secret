/**
 * Created by youngkim on 2014. 11. 12..
 */

'use strict';

var mysqlConn = require(SOURCE_ROOT + '/module/mysql').getConnection();
var crypto = require('crypto');
var async = require('async');

/*
 * 로그인 페이지를 보여준다.
 */
exports.viewLoginPage = function (req, res) {
    res.render('login');
};

/*
 * 일반 유저의 정보를 세팅해준다.
 */
var setUserSession = function (req, email, callback) {
    req.session.isAdmin = false;
    req.session.loginStatus = true;
    req.session.useremail = email;
    mysqlConn.query('SELECT nickname FROM user WHERE email = ?', [email], function (err, result) {
        if (err) {
            throw err;
        } else {
            req.session.nickname = result[0].nickname;
            callback();
        }
    });
};

/*
 * 관리자 정보를 세팅해준다. (2014.12.2 현재 사용하지 않음)
 */
var setAdminSession = function (req, email, callback) {
    req.session.isAdmin = true;
    req.session.loginStatus = true;
    req.session.useremail = email;
    mysqlConn.query('SELECT nickname FROM user WHERE email = ?', [email], function (err, result) {
        if (err) {
            throw err;
        } else {
            req.session.nickname = result[0].nickname;
            callback();
        }
    });
};

/*
 * 로그인 정보를 받아 처리한다.
 */
exports.handleLoginRequest = function (req, res) {
    var email = req.body.email,
        password = req.body.password;

    mysqlConn.query(
        'SELECT email, password, user_type FROM user WHERE email = ?', [email], function (err, result) {
            if (err) throw err;
            if (result.length === 0) {
                res.render('error', {message: "Your E-mail or PW is incorrect. Please check again!"});
            }
            else {
                var hashpassword = crypto.createHash('sha512').update(password).digest('hex');

                if (result[0].password === hashpassword) {
                    if (result[0].user_type === '0') {
                        //인증 안한 사용자
                        res.render('error', {message: "Please verify your E-mail"});
                    }
                    else {
                        if (result[0].user_type === '1') {
                            // 인증한 사용자
                            setUserSession(req, email, function () {
                                res.redirect('/upload');
                            });
                        }
                        else {
                            // 관리자
                            setAdminSession(req, email, function () {
                                res.redirect('/upload');
                            });
                        }
                    }
                }
                else {
                    res.render('error', {message: "Your E-mail or PW is incorrect. Please check again!"});
                }
            }
        }
    );
};

/*
 * 유저가 로그아웃하게 한다. 쿠키 및 세션 정보를 비워준다.
 */
exports.handleLogoutRequest = function (req, res) {
    req.session.isAdmin = false;
    req.session.loginStatus = false;
    req.session.useremail = undefined;
    req.session.nickname = undefined;

    res.clearCookie('googleplus', {path: '/user'});
    res.clearCookie('facebook', {path: '/user'});

    res.cookie('googleplus', 'false', {maxAge: 900000, httpOnly: true});
    res.cookie('facebook', 'false', {maxAge: 900000, httpOnly: true});

    res.redirect('/user/login');
};

/*
 * 가입 페이지를 보여준다.
 */
exports.viewJoinPage = function (req, res) {
    res.render('join', {login: false});
};

/*
 * 가입 정보를 받아 처리한다.
 */
exports.handleJoinRequest = function (req, res) {
    var email = req.body.email,
        password = req.body.password,

    //  to convert Date to JSON Data
        now = new Date(),
        jsonDate = now.toJSON(),
        time = new Date(jsonDate);

    //  make hashed Password for security
    var hashpassword = crypto.createHash('sha512').update(password).digest('hex'),
        hashAuthKey = crypto.createHash('sha512').update(email).digest('hex');

    var userData = {
        email: email,
        nickname: email.split('@')[0] + crypto.createHash('sha512').update(email).digest('hex').toString().slice(0, 3),
        password: hashpassword,
        profile_url: 0,
        homepage: '',
        facebook: '',
        facebook_login: 0,
        googleplus: '',
        googleplus_login: 0,
        gallery: '',
        address: 'Address',
        introduction: '',
        join_time: time,
        user_type: '0'
    };

    var userAuth = {
        user_email: email,
        user_key: hashAuthKey
    };

    //  Email validate
    var emailRegExp = /.+\@.+\..+/;

    if (!emailRegExp.test(email)) {
        res.render('error', {message: 'It\'s not a E-mail format'})
    }
    else {
        mysqlConn.query(
            'INSERT INTO user SET ?', userData, function (err) {
                if (err) {
                    res.render('error', {message: 'E-mail is already in use!'});
                }
                else {
                    mysqlConn.query(
                        'INSERT INTO user_auth SET ?', userAuth, function (err) {
                            if (err) throw err;
                            else {
                                sendJoinAuthMail(userData, userAuth);
                                res.render('error', {
                                    message: 'Verification mail’s been sent to your E-mail. ' +
                                    'Please check your Inbox and Spam folder!', login: false
                                });
                                //res.render('error', {message: '업로드 홧팅하세요~~'});
                            }
                        }
                    );
                }
            }
        );
    }
};

/*
 * 가입 인증 메일을 보내준다.
 */
var sendJoinAuthMail = function (userData, userAuth) {
    var mailModule = require(SOURCE_ROOT + '/module/mail');
    var mailConn = mailModule.createConn();

    var mailURL = SETTING.url + "/user/mailAuth/" + userAuth.user_key;

    var mailOptions = {
        from: "Mushroom <info@mushroom.land>", // sender address
        to: userData.email, // list of receivers
        subject: "Mushroom Account Verification Mail", // Subject line
        html: "Click <a href = " + mailURL + ">HERE</a>" + " to verify your account!"
    };

    mailConn.sendMail(mailOptions, function (error, response) {
        if (error) {
            error('error occurred : ' + error + 'while send ' + userData.email + ' auth key');
            mysqlConn.query('DELETE FROM user_auth WHERE user_email = ?', [userData.email], function (err) {
                if (err) {
                    error('error occurred : ' + error + 'while send ' + userData.email + ' auth key');
                    throw err;
                }
                error('delete ' + userData.email + ' auth key');
            });
            mysqlConn.query('DELETE FROM user WHERE email = ?', [userData.email], function (err) {
                if (err) {
                    error('error occurred : ' + error + 'while send ' + userData.email + ' auth key');
                    throw err;
                }
                error('delete ' + userData.email + ' user info');
            });
        } else {
            info("Message sent to : " + userData.email);
        }
    });
};

/*
 * 가입 인증 메일을 클릭하면 user_auth 테이블을 확인하고, 유저 정보를 업데이트 한다.
 */
exports.userMailAuth = function (req, res) {
    var authKey = req.params.authKey;

    mysqlConn.query(
        'SELECT user_email FROM user_auth WHERE user_key = ?', [authKey], function (err, result) {
            if (err) throw err;
            else {
                if (result.length === 0) {
                    res.render('error', {message: "Your E-mail is already verified"});
                }
                else {
                    var userMail = result[0].user_email;

                    mysqlConn.query(
                        'UPDATE user SET user_type = \'1\' WHERE email = ?', [userMail], function (err) {
                            if (err) throw err;
                            else {
                                mysqlConn.query(
                                    'DELETE FROM user_auth WHERE user_key = ?', [authKey], function (err) {
                                        if (err) {
                                            error("user_email = " + userMail + "의 AuthKey가 삭제되지 않음");
                                            throw err;
                                        }
                                        else {
                                            setUserSession(req, userMail, function () {
                                                res.render('error', {
                                                    message: "Verification Complete! Enjoy!", login: true,
                                                    nickname: req.session.nickname
                                                });
                                            });
                                        }
                                    }
                                );
                            }
                        }
                    );
                }
            }
        }
    );
};

/*
 * 프로필 페이지를 보여준다.
 */
exports.viewProfilePage = function (req, res) {
    mysqlConn.query('SELECT * FROM user WHERE email =?', [req.session.useremail], function (err, result) {
        if (err) throw err;

        var renderData = {
            userData: result[0],
            login: true,
            nickname: req.session.nickname
        };

        for (var index in renderData.userData) {
            if (renderData.userData.hasOwnProperty(index)) {
                var attr = renderData.userData[index];
                if (attr === '' || attr === null) {
                    renderData.userData[index] = '';
                }
            }
        }

        if (renderData.userData.googleplus_login === 1) {
            renderData.googleplus_link = true;
            renderData.googleplus_address = renderData.userData.googleplus;
        } else {
            renderData.googleplus_link = false;
            renderData.googleplus_address = '';
        }
        if (renderData.userData.facebook_login === 1) {
            renderData.facebook_link = true;
            renderData.facebook_address = renderData.userData.facebook;
        } else {
            renderData.facebook_link = false;
            renderData.facebook_address = '';
        }

        res.render('profile', renderData);
    });
};

/*
 * 프로필 수정 사항 정보를 받아 처리한다.
 */
exports.handleProfileRequest = function (req, res) {
    var userData = {
        address: req.body.address || '',
        homepage: req.body.homepage || '',
        gallery: req.body.gallery || '',
        introduction: req.body.introduction || '',
        nickname: req.body.nickname || ''
    };

    if (userData.introduction != '') {
        userData.introduction.replace(/\n/g, '<br>');
    }

    var renderData = {
        message: 'Error occurred while change your profile page. sorry, please try again',
        login: true,
        nickname: req.session.nickname
    };

    async.waterfall([
        function (callback) {
            if (req.body.oldPassword != undefined && req.body.newPassword != undefined && req.body.newPassword === req.body.rePassword) {
                mysqlConn.query('SELECT password FROM user WHERE email =?', [req.session.useremail], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        if (result[0].password === crypto.createHash('sha512').update(req.body.oldPassword).digest('hex')) {
                            userData.password = crypto.createHash('sha512').update(req.body.newPassword).digest('hex');
                            callback(null);
                        }
                        else {
                            renderData.message = "Please type PW again!";
                            callback(new Error("Please type PW again!"));
                        }
                    }
                });
            } else {
                callback(null);
            }
        },
        function (callback) {
            if (userData.nickname != req.session.nickname) {
                mysqlConn.query('SELECT nickname FROM user WHERE nickname = ?', [userData.nickname], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        if (result.length === 0) {
                            renderData.nickname = userData.nickname;
                            req.session.nickname = userData.nickname;
                            callback(null);
                        } else {
                            renderData.message = "That nickname is already in use!";
                            callback(new Error("That nickname is already in use!"));
                        }
                    }
                });
            } else {
                callback(null);
            }
        },
        function (callback) {
            if (req.body.profilePicture != undefined) {
                userData.profile_url = req.body.profilePicture;
            }
            mysqlConn.query('UPDATE user SET ? WHERE email = ?', [userData, req.session.useremail], function (err) {
                if (err) {
                    callback(err);
                } else {
                    callback(null);
                }
            });
        }
    ], function (err) {
        if (err) {
            error('error occurred : ' + err + 'while handle profile request ////' + req.session.useremail + ' profile and artwork');
            res.render('error', renderData);
        } else {
            renderData.message = 'Successfully changed your profile';
            res.render('error', renderData);
        }
    });
};

/*
 * 현재 로그인 유저의 마스터 페이지를 보여준다.
 */
exports.viewCurUserPage = function (req, res) {
    var renderData = {
        userData: undefined,
        artworks: undefined,
        artwork_first: {},
        login: true,
        nickname: req.session.nickname,
        message: 'Error occurred while load your master page. sorry, please try again'
    };

    async.waterfall([
            function (callback) {
                mysqlConn.query('SELECT id, email, nickname, profile_url, homepage, facebook, facebook_login, ' +
                'googleplus, googleplus_login, address, gallery, introduction FROM user WHERE email =?', [req.session.useremail], function (err, result) {
                    if (err) {
                        callback(err, null);
                    }
                    else {
                        renderData.userData = result[0];

                        for (var index in renderData.userData) {
                            if (renderData.userData.hasOwnProperty(index)) {
                                var attr = renderData.userData[index];
                                if (attr === '' || attr === null) {
                                    renderData.userData[index] = '';
                                }
                            }
                        }

                        if (renderData.userData.googleplus_login === 1) {
                            renderData.googleplus_link = true;
                            renderData.googleplus_address = renderData.userData.googleplus;
                        } else {
                            renderData.googleplus_link = false;
                            renderData.googleplus_address = '';
                        }
                        if (renderData.userData.facebook_login === 1) {
                            renderData.facebook_link = true;
                            renderData.facebook_address = renderData.userData.facebook;
                        } else {
                            renderData.facebook_link = false;
                            renderData.facebook_address = '';
                        }

                        callback(null, result[0].id);
                    }
                });
            },
            function (user, callback) {
                mysqlConn.query('SELECT * FROM artwork WHERE artist =? ORDER BY id DESC',
                    [user], function (err, result) {
                        if (err) {
                            callback(err, null);
                        } else {
                            if (result.length === 0) {
                                renderData.message = 'Please upload at least 1 artwork to check your master page';
                                callback(new Error(' no artwork exist '), null);
                            } else {
                                renderData.artworks = result;
                                callback(null, result[0]);
                            }
                        }
                    });
            },
            function (artwork_first, callback) {
                renderData.artwork_first.info = artwork_first;

                if (renderData.artwork_first.info.title === '') {
                    renderData.artwork_first.info.title = 'No Title';
                }
                if (renderData.artwork_first.info.width === '') {
                    //renderData.artwork_first.info.width = 'W';
                }
                if (renderData.artwork_first.info.height === '') {
                    //renderData.artwork_first.info.height = 'H';
                }
                if (renderData.artwork_first.info.depth === '') {
                    //renderData.artwork_first.info.depth = 'D';
                }
                mysqlConn.query('SELECT tag, id FROM artwork_has_hashtag JOIN hashtag ' +
                    'WHERE artwork_has_hashtag.artwork_id = ? AND artwork_has_hashtag.hashtag_id = hashtag.id',
                    [artwork_first.id], function (err, result) {
                        if (err) {
                            callback(err, null);
                        } else {
                            if (result.length === 0) {
                                renderData.artwork_first.hashtag1 = {tag: '', id: ''};
                                renderData.artwork_first.hashtag2 = {tag: '', id: ''};
                                renderData.artwork_first.hashtag3 = {tag: '', id: ''};
                                renderData.artwork_first.hashtag4 = {tag: '', id: ''};
                                renderData.artwork_first.hashtag5 = {tag: '', id: ''};
                            } else {
                                renderData.artwork_first.hashtag1 = result[0] || {tag: '', id: ''};
                                renderData.artwork_first.hashtag2 = result[1] || {tag: '', id: ''};
                                renderData.artwork_first.hashtag3 = result[2] || {tag: '', id: ''};
                                renderData.artwork_first.hashtag4 = result[3] || {tag: '', id: ''};
                                renderData.artwork_first.hashtag5 = result[4] || {tag: '', id: ''};
                            }
                            callback(null, artwork_first);
                        }
                    });
            },
            function (artwork_first, callback) {
                mysqlConn.query('SELECT ac.id, ac.artwork_id, ac.user_id, ac.content, DATE_FORMAT(ac.comment_time, \'%Y%m%d\') as comment_time, u.nickname FROM artwork_comment ac ' +
                    'JOIN user u WHERE ac.user_id = u.id AND ac.artwork_id = ?',
                    [artwork_first.id], function (err, result) {
                        if (err) {
                            callback(err, null);
                        } else {
                            renderData.artwork_first.comments = result;
                            callback(null, artwork_first);
                        }
                    });
            },
            function (artwork_first, callback) {
                mysqlConn.query('SELECT * FROM user_relation_artwork WHERE artwork_id =? AND user_id =?',
                    [artwork_first.id, renderData.userData.id], function (err, result) {
                        if (err) {
                            callback(err, null);
                        } else {
                            if (result.length === 0 || result[0].like_flag === 0) {
                                renderData.artwork_first.like = false;
                            } else {
                                renderData.artwork_first.like = true;
                            }
                            callback(null, artwork_first);
                        }
                    });
            }
        ],
        function (err) {
            if (err) {
                error('error occurred : ' + err + ' //// while view cur page ' + req.session.useremail + ' /// user page');
                res.render('error', renderData);
            } else {
                res.render('mypage', renderData);
            }
        }
    );
};

/*
 * 유저의 마스터 페이지를 보여준다.
 */
exports.viewMasterPage = function (req, res) {
    var artistId = req.params.artist_id;
    var userId;

    var renderData = {
        userData: undefined,
        artworks: undefined,
        artwork_first: {},
        login: true,
        nickname: req.session.nickname,
        message: 'Error occurred while load artist master page. sorry, please re-login and try again'
    };

    async.waterfall([
            function (callback) {
                mysqlConn.query('SELECT id FROM user WHERE email =?', [req.session.useremail], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        if (result.length === 0) {
                            callback(new Error(' invalid user id '));
                        } else {
                            userId = result[0].id;
                            callback(null);
                        }
                    }
                });
            },
            function(callback) {
                mysqlConn.query('SELECT * FROM user_subscribe_artist WHERE user_id = ? AND artist_id = ?',
                    [userId, artistId], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        if (result.length != 0) {
                            renderData.subscribe = true;
                            callback(null);
                        } else {
                            callback(null);
                        }
                    }
                });
            },
            function (callback) {
                mysqlConn.query('SELECT id, email, nickname, profile_url, homepage, facebook, facebook_login, ' +
                'googleplus, googleplus_login, address, gallery, introduction FROM user WHERE id =?', [artistId], function (err, result) {
                    if (err) {
                        callback(err, null);
                    }
                    else {
                        if (result.length === 0) {
                            renderData.message = 'invalid artist id';
                            callback(new Error(' invalid artist id '), null);
                        } else {
                            renderData.userData = result[0];

                            for (var index in renderData.userData) {
                                if (renderData.userData.hasOwnProperty(index)) {
                                    var attr = renderData.userData[index];
                                    if (attr === '' || attr === null) {
                                        renderData.userData[index] = '';
                                    }
                                }
                            }

                            if (renderData.userData.googleplus_login === 1) {
                                renderData.googleplus_link = true;
                                renderData.googleplus_address = renderData.userData.googleplus;
                            } else {
                                renderData.googleplus_link = false;
                                renderData.googleplus_address = '';
                            }
                            if (renderData.userData.facebook_login === 1) {
                                renderData.facebook_link = true;
                                renderData.facebook_address = renderData.userData.facebook;
                            } else {
                                renderData.facebook_link = false;
                                renderData.facebook_address = '';
                            }

                            callback(null, result[0].id);
                        }
                    }
                });
            },
            function (user, callback) {
                mysqlConn.query('SELECT * FROM artwork WHERE artist =? ORDER BY id DESC',
                    [artistId], function (err, result) {
                        if (err) {
                            callback(err, null);
                        } else {
                            if (result.length === 0) {
                                renderData.message = 'this artist didn\'t upload any artwork';
                                callback(new Error('no artwork exist'), null);
                            } else {
                                renderData.artworks = result;
                                callback(null, result[0]);
                            }
                        }
                    });
            },
            function (artwork_first, callback) {
                renderData.artwork_first.info = artwork_first;

                if (renderData.artwork_first.info.title === '') {
                    renderData.artwork_first.info.title = 'No Title';
                }
                if (renderData.artwork_first.info.width === '') {
                    //renderData.artwork_first.info.width = 'W';
                }
                if (renderData.artwork_first.info.height === '') {
                    //renderData.artwork_first.info.height = 'H';
                }
                if (renderData.artwork_first.info.depth === '') {
                    //renderData.artwork_first.info.depth = 'D';
                }
                mysqlConn.query('SELECT tag, id FROM artwork_has_hashtag JOIN hashtag ' +
                    'WHERE artwork_has_hashtag.artwork_id = ? AND artwork_has_hashtag.hashtag_id = hashtag.id',
                    [artwork_first.id], function (err, result) {
                        if (err) {
                            callback(err, null);
                        } else {
                            if (result.length === 0) {
                                renderData.artwork_first.hashtag1 = {tag: '', id: ''};
                                renderData.artwork_first.hashtag2 = {tag: '', id: ''};
                                renderData.artwork_first.hashtag3 = {tag: '', id: ''};
                                renderData.artwork_first.hashtag4 = {tag: '', id: ''};
                                renderData.artwork_first.hashtag5 = {tag: '', id: ''};
                            } else {
                                renderData.artwork_first.hashtag1 = result[0] || {tag: '', id: ''};
                                renderData.artwork_first.hashtag2 = result[1] || {tag: '', id: ''};
                                renderData.artwork_first.hashtag3 = result[2] || {tag: '', id: ''};
                                renderData.artwork_first.hashtag4 = result[3] || {tag: '', id: ''};
                                renderData.artwork_first.hashtag5 = result[4] || {tag: '', id: ''};
                            }
                            callback(null, artwork_first);
                        }
                    });
            },
            function (artwork_first, callback) {
                mysqlConn.query('SELECT ac.id, ac.artwork_id, ac.user_id, ac.content, DATE_FORMAT(ac.comment_time, \'%Y%m%d\') as comment_time, u.nickname FROM artwork_comment ac ' +
                    'JOIN user u WHERE ac.user_id = u.id AND ac.artwork_id = ?',
                    [artwork_first.id], function (err, result) {
                        if (err) {
                            callback(err, null);
                        } else {
                            renderData.artwork_first.comments = result;
                            callback(null, artwork_first);
                        }
                    });
            },
            function (artwork_first, callback) {
                mysqlConn.query('SELECT * FROM user_relation_artwork WHERE artwork_id =? AND user_id =?',
                    [artwork_first.id, userId], function (err, result) {
                        if (err) {
                            callback(err, null);
                        } else {
                            if (result.length === 0 || result[0].like_flag === 0) {
                                renderData.artwork_first.like = false;
                            } else {
                                renderData.artwork_first.like = true;
                            }
                            callback(null, artwork_first);
                        }
                    });
            }
        ],
        function (err) {
            if (err) {
                error('error occurred : ' + err + ' //// while view artist page ' + req.session.useremail + ' /// user page');
                res.render('error', renderData);
            } else {
                res.render('master', renderData);
            }
        }
    );
};

/*
 * 현재 로그인 유저의 정보를 보내준다. (Ajax용)
 */
exports.getUserId = function (req, res) {
    mysqlConn.query('SELECT id FROM user WHERE email = ?', [req.session.useremail], function (err, result) {
        if (err) {
        } else {
            var data = result[0];
            res.type('application/json');
            res.status(200).send(JSON.stringify(data));
        }
    });
};

/*
 * 현재 로그인 유저가 다른 유저를 subscribe 한다.
 */
exports.handleSubscribeRequest = function (req, res) {
    var user_id = req.body.user_id;
    var artist_id = req.body.artist_id;

    async.waterfall([
        function (callback) {
            mysqlConn.query('SELECT * FROM user_subscribe_artist WHERE user_id = ? AND artist_id = ?', [user_id, artist_id], function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    if (result.length != 0) {
                        callback(null, false);
                    } else {
                        callback(null, true);
                    }
                }
            });
        },
        function (flag, callback) {
            if (flag === false) {
                callback(null);
            } else {
                mysqlConn.query('INSERT INTO user_subscribe_artist (user_id, artist_id) VALUES (?, ?)', [user_id, artist_id], function (err) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null);
                    }
                });
            }
        }
    ], function (err) {
        if (err) {
            res.status(500).send();
        } else {
            res.status(200).send();
        }
    });
};

/*
 * 현재 로그인 유저가 다른 유저를 unSubscribe 한다.
 */
exports.handleUnSubscribeRequest = function (req, res) {
    var user_id = req.body.user_id;
    var artist_id = req.body.artist_id;

    mysqlConn.query('DELETE FROM user_subscribe_artist WHERE user_id = ? AND artist_id = ?', [user_id, artist_id], function (err) {
        if (err) {
            res.status(500).send();
        } else {
            res.status(200).send();
        }
    });
};


/**
 * function for sns login, link
 */
var userService = require(SOURCE_ROOT + '/module/user');

/**
 * If there is user in database, login user.
 * If not, register user.
 */
exports.loginGoogle = function (req, res) {
    var userData = req.body.userData;

    userService.handleGooglePlusUser(req, res, userData);
};

exports.loginFacebook = function (req, res) {
    var userData = req.body.userData;

    userService.handleFacebookUser(req, res, userData);
};

/*
 * 유저의 정보에 구글 정보를 추가한다.
 */
exports.linkGoogle = function (req, res) {
    var userData = req.body.userData;

    var renderData = {
        message: undefined,
        login: true,
        nickname: req.session.nickname
    };

    var googlePlusData = {
        googleplus_login: 1,
        googleplus: userData.link || undefined
    };

    if (googlePlusData.googleplus === undefined && googlePlusData.googleplus === '') {
        renderData.message = 'GooglePlus link failed. please login to googleplus and try again';
        res.render('error', renderData);
    } else {
        mysqlConn.query('UPDATE user SET ? WHERE email = ?', [googlePlusData, req.session.useremail], function (err) {
            res.status(200).send();
        });
    }
};

/*
 * 유저의 정보에 페이스북 정보를 추가한다.
 */
exports.linkFacebook = function (req, res) {
    var userData = req.body.userData;

    var renderData = {
        message: undefined,
        login: true,
        nickname: req.session.nickname
    };

    var facebookData = {
        facebook_login: 1,
        facebook: 'http://www.facebook.com/' + userData.id || undefined
    };

    if (userData.id === undefined || userData.id === '') {
        renderData.message = 'Facebook link failed. please login to facebook and try again';
        res.render('error', renderData);
    } else {
        mysqlConn.query('UPDATE user SET ? WHERE email = ?', [facebookData, req.session.useremail], function (err) {
            res.status(200).send();
        });
    }
};