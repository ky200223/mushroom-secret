/**
 * Created by youngkim on 2014. 11. 6..
 */

'use strict';

var mysqlConn = require(SOURCE_ROOT + '/module/mysql').getConnection();
var async = require('async');

/*
 * recommend page를 보여준다.
 */
exports.viewRecommendPage = function (req, res) {
//    res.render('error', {message: 'Coming soon!', login: true});
    res.render('recommend', {login: true, nickname: req.session.nickname});
};

/*
 * recommend.js 에서 보낸 artwork id를 조회하여 해당 정보를 return
 */
exports.handleLoadMoreRequest = function (req, res) {
    var id_1 = req.body.id_1;
    var id_2 = req.body.id_2;
    var id_3 = req.body.id_3;

    async.parallel([
        function (callback) {
            mysqlConn.query('SELECT * FROM artwork WHERE id = ?', [id_1], function (err, result) {
                if (err) {
                    callback(err, null);
                }
                callback(null, result[0]);
            });
        },
        function (callback) {
            mysqlConn.query('SELECT * FROM artwork WHERE id = ?', [id_2], function (err, result) {
                if (err) {
                    callback(err, null);
                }
                callback(null, result[0]);
            });
        },
        function (callback) {
            mysqlConn.query('SELECT * FROM artwork WHERE id = ?', [id_3], function (err, result) {
                if (err) {
                    callback(err, null);
                }
                callback(null, result[0]);
            });
        }
    ], function (err, results) {
        if (err) throw err;
        var data = {
            artwork_1: results[0],
            artwork_2: results[1],
            artwork_3: results[2]
        };
        res.status(200).send(data);
    });
};

//exports.viewFullArtworkPage = function (req, res) {
//    var renderData = {
//        info: {
//            userId: req.query.user || undefined,
//            artworkId: req.query.artwork || undefined,
//            artistId: req.query.artist || undefined,
//            image_url: req.query.image || undefined
//        },
//        login: true,
//        nickname: req.session.nickname
//    };
//
//    if (renderData.info.userId === undefined || renderData.info.artworkId === undefined
//        || renderData.info.artistId === undefined || renderData.info.image_url === undefined) {
//        renderData.info = undefined;
//        renderData.message = 'Bad Url. Please try again';
//        res.render('error', renderData);
//    } else {
//        res.render('fullArtwork', renderData);
//    }
//};