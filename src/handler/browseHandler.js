/**
 * Created by youngkim on 2014. 11. 12..
 */

'use strict';

var mysqlConn = require(SOURCE_ROOT + '/module/mysql').getConnection();
var async = require('async');

/*
 * browse 페이지를 보여준다.
 */
exports.viewBrowsePage = function (req, res) {
    //res.render('error', {message: 'Coming soon!', login: true, nickname: req.session.nickname});
    var searchFlag = req.query.searchFlag;
    var searchText = req.query.searchText;

    var renderData = {
        result: undefined,
        login: true,
        nickname: req.session.nickname,
        end: false
    };

    var findAllArtwork = function (callback) {
        async.waterfall([
            function (callback) {
                mysqlConn.query('SELECT id FROM user WHERE email=?', [req.session.useremail], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, result[0].id);
                    }
                });
            }, function (userId, callback) {
                mysqlConn.query('SELECT IF(like_rel.rel_flag = 1, 1, 0) like_flag, user.nickname nickname, ' +
                    'art.introduction introduction, art.thumbnail_url thumbnail_url, art.id id, art.artist artistId, ' +
                    'art.title title FROM artwork art LEFT JOIN (SELECT rel.artwork_id rel_art, rel.like_flag rel_flag ' +
                    'FROM user_relation_artwork rel WHERE rel.user_id = ?) like_rel ON art.id = like_rel.rel_art ' +
                    'JOIN user user ON user.id = art.artist ORDER by art.upload_time DESC LIMIT 19',
                    [userId], function (err, result) {
                        if (err) {
                            callback(err);
                        } else {
                            renderData.result = result;
                            callback(null);
                        }
                    });
            }
        ], function (err) {
            if (err) {
                renderData.message = 'find all artwork flow failed. please try again';
                res.render('error', renderData);
            } else {
                if (renderData.result.length <= 18) {
                    renderData.end = true;
                } else {
                    renderData.result = renderData.result.slice(0, 18);
                }
                callback();
            }
        });
    };

    var findArtwork = function (callback) {
        async.waterfall([
            function (callback) {
                mysqlConn.query('SELECT id FROM user WHERE email=?', [req.session.useremail], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, result[0].id);
                    }
                })
            }, function (userId, callback) {
                mysqlConn.query('SELECT IF(rel.like_flag REGEXP \'^[0-9]+$\', 1, 0) like_flag, ' +
                    'user.nickname nickname, art.thumbnail_url thumbnail_url, art.introduction introduction, ' +
                    'art.id id, hash.tag tag, art.artist artistId ' +
                    'FROM hashtag hash JOIN artwork_has_hashtag art_hash ON hash.id = art_hash.hashtag_id ' +
                    'JOIN artwork art ON art.id = art_hash.artwork_id ' +
                    'JOIN user user ON art.artist = user.id ' +
                    'LEFT JOIN (SELECT * FROM user_relation_artwork rel WHERE rel.user_id = ? ) rel ' +
                    'ON art.id = rel.artwork_id ' +
                    'WHERE (art.title LIKE ?) OR (hash.tag LIKE ?) ' +
                    'GROUP BY art.id ORDER by art.upload_time DESC LIMIT 19',
                    [userId, '%' + searchText + '%', '%' + searchText + '%'], function (err, result) {
                        if (err) {
                            callback(err);
                        } else {
                            renderData.result = result;
                            callback(null);
                        }
                    });
            }, function (callback) {
                mysqlConn.query('SELECT id, search_count FROM hashtag WHERE tag LIKE ? LIMIT 1',
                    ['%' + searchText + '%'], function (err, result) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, result);
                        }
                    });
            }, function (result, callback) {
                if (result.length != 0) {
                    mysqlConn.query('UPDATE hashtag SET search_count = ? WHERE id = ?',
                        [parseInt(result[0].search_count) + 1, result[0].id], function (err, result) {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, result);
                            }
                        });
                } else {
                    callback(null);
                }
            }
        ], function (err) {
            if (err) {
                renderData.message = 'find all artwork flow failed. please try again';
                res.render('error', renderData);
            } else {
                if (renderData.result.length <= 18) {
                    renderData.end = true;
                } else {
                    renderData.result = renderData.result.slice(0, 18);
                }
                callback();
            }
        });
    };

    var findAllArtist = function (callback) {
        async.waterfall([
            function (callback) {
                mysqlConn.query('SELECT id FROM user WHERE email=?', [req.session.useremail], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, result[0].id);
                    }
                });
            }, function (userId, callback) {
                mysqlConn.query('SELECT userT.id id, userT.profile_url profile_url, userT.nickname nickname, ' +
                    'IF(subscribe_artist.id REGEXP \'^[0-9]+$\', 1, 0) sub_id ' +
                    'FROM user userT LEFT JOIN (SELECT user_subscribe_artist.artist_id id ' +
                    'FROM user user JOIN user_subscribe_artist ON user.id = user_subscribe_artist.user_id ' +
                    'WHERE user.id = ?) subscribe_artist ON userT.id = subscribe_artist.id ' +
                    'ORDER by userT.join_time DESC LIMIT 19',
                    [userId], function (err, result) {
                        if (err) {
                            callback(err);
                        } else {
                            renderData.result = result;
                            callback(null);
                        }
                    });
            }
        ], function (err) {
            if (err) {
                renderData.message = 'find all artist flow failed. please try again';
                res.render('error', renderData);
            } else {
                if (renderData.result.length <= 18) {
                    renderData.end = true;
                } else {
                    renderData.result = renderData.result.slice(0, 18);
                }
                callback();
            }
        });
    };

    var findArtist = function (callback) {
        async.waterfall([
            function (callback) {
                mysqlConn.query('SELECT id FROM user WHERE email=?', [req.session.useremail], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, result[0].id);
                    }
                });
            }, function (userId, callback) {
                mysqlConn.query('SELECT userT.id id, userT.profile_url profile_url, userT.nickname nickname, ' +
                    'IF(subscribe_artist.id REGEXP \'^[0-9]+$\', 1, 0) sub_id FROM user userT ' +
                    'LEFT JOIN (SELECT user_subscribe_artist.artist_id id FROM user user ' +
                    'JOIN user_subscribe_artist ON user.id = user_subscribe_artist.user_id ' +
                    'WHERE user.id = ?) subscribe_artist ON userT.id = subscribe_artist.id ' +
                    'WHERE userT.nickname LIKE ? OR userT.email LIKE ? ORDER by userT.join_time DESC LIMIT 19;',
                    [userId, '%' + searchText + '%', '%' + searchText + '%'], function (err, result) {
                        if (err) {
                            callback(err);
                        } else {
                            renderData.result = result;
                            callback(null);
                        }
                    });
            }, function (callback) {
                mysqlConn.query('SELECT id, search_count FROM user WHERE nickname LIKE ? OR email LIKE ? LIMIT 3',
                    ['%' + searchText + '%', '%' + searchText + '%'], function (err, result) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, result);
                        }
                    });
            }, function (result, callback) {
                if (result.length != 0) {
                    if (result.length >= 1) {
                        mysqlConn.query('UPDATE user SET search_count = search_count + 1 WHERE id = ?',
                            [result[0].id], function (err) {
                                if (err) {
                                    callback(err);
                                } else {
                                    callback(null);
                                }
                            });
                    }
                    if (result.length >= 2) {
                        mysqlConn.query('UPDATE user SET search_count = search_count + 1 WHERE id = ?',
                            [result[1].id], function (err) {
                                if (err) {
                                    callback(err);
                                } else {
                                    callback(null);
                                }
                            });
                    }
                    if (result.length >= 3) {
                        mysqlConn.query('UPDATE user SET search_count = search_count + 1 WHERE id = ?',
                            [result[2].id], function (err) {
                                if (err) {
                                    callback(err);
                                } else {
                                    callback(null);
                                }
                            });
                    }
                } else {
                    callback(null);
                }
            }
        ], function (err) {
            if (err) {
                console.log(err);
                renderData.message = 'find artist flow failed. please try again';
                res.render('error', renderData);
            } else {
                if (renderData.result.length <= 18) {
                    renderData.end = true;
                } else {
                    renderData.result = renderData.result.slice(0, 18);
                }
                callback();
            }
        });
    };

    if (searchFlag === undefined) {
        res.redirect('/browse/?searchFlag=artwork');
    } else if (searchFlag != 'artwork' && searchFlag != 'artist') {
        res.redirect('/browse/?searchFlag=artwork');
    } else if (searchFlag === 'artwork' && searchText === undefined) {
        findAllArtwork(function () {
            res.render('browseArtwork', renderData);
        });
    } else if (searchFlag === 'artwork' && searchText != undefined) {
        findArtwork(function () {
            res.render('browseArtwork', renderData);
        });
    } else if (searchFlag === 'artist' && searchText === undefined) {
        findAllArtist(function () {
            res.render('browseArtist', renderData);
        });
    } else if (searchFlag === 'artist' && searchText != undefined) {
        findArtist(function () {
            res.render('browseArtist', renderData);
        });
    }
};

/*
 * browse - all artwork의 정보를 ajax로 가져온다.
 */
exports.handleAllArtworkQueryRequest = function (req, res) {
    var sendIndex = parseInt(req.body.sendIndex);

    var sendData = {
        end: false,
        result: undefined,
        flag: 'artwork'
    };

    var findAllArtwork = function (callback) {
        async.waterfall([
            function (callback) {
                mysqlConn.query('SELECT id FROM user WHERE email=?', [req.session.useremail], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, result[0].id);
                    }
                });
            }, function (userId, callback) {
                mysqlConn.query('SELECT IF(like_rel.rel_flag = 1, 1, 0) like_flag, user.nickname nickname, ' +
                    'art.introduction introduction, art.thumbnail_url thumbnail_url, art.id id, art.artist artistId ' +
                    'FROM artwork art LEFT JOIN (SELECT rel.artwork_id rel_art, rel.like_flag rel_flag ' +
                    'FROM user_relation_artwork rel WHERE rel.user_id = ?) like_rel ON art.id = like_rel.rel_art ' +
                    'JOIN user user ON user.id = art.artist ORDER by art.upload_time DESC LIMIT ?,19',
                    [userId, sendIndex], function (err, result) {
                        if (err) {
                            callback(err);
                        } else {
                            sendData.result = result;
                            callback(null);
                        }
                    });
            }
        ], function (err) {
            if (err) {
                res.status(500).send();
            } else {
                if (sendData.result.length <= 18) {
                    sendData.end = true
                } else {
                    sendData.result = sendData.result.slice(0, 18);
                }
                callback();
            }
        });
    };

    findAllArtwork(function () {
        res.status(200).send(sendData);
    });
};

/*
 * browse - artwork의 정보를 ajax로 가져온다.
 */
exports.handleArtworkQueryRequest = function (req, res) {
    var searchText = req.params.searchText;
    var sendIndex = parseInt(req.body.sendIndex);

    var sendData = {
        end: false,
        result: undefined,
        flag: 'artwork'
    };

    var findArtwork = function (callback) {
        async.waterfall([
            function (callback) {
                mysqlConn.query('SELECT id FROM user WHERE email=?', [req.session.useremail], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, result[0].id);
                    }
                })
            }, function (userId, callback) {
                mysqlConn.query('SELECT IF(rel.like_flag REGEXP \'^[0-9]+$\', 1, 0) like_flag, ' +
                    'user.nickname nickname, art.thumbnail_url thumbnail_url, art.introduction introduction, ' +
                    'art.id, hash.tag tag, art.artist artistId ' +
                    'FROM hashtag hash JOIN artwork_has_hashtag art_hash ON hash.id = art_hash.hashtag_id ' +
                    'JOIN artwork art ON art.id = art_hash.artwork_id ' +
                    'JOIN user user ON art.artist = user.id ' +
                    'LEFT JOIN (SELECT * FROM user_relation_artwork rel WHERE rel.user_id = ? ) rel ' +
                    'ON art.id = rel.artwork_id WHERE (art.title LIKE ?) OR (hash.tag LIKE ?) ' +
                    'GROUP BY art.id ORDER by art.upload_time DESC LIMIT ?, 19',
                    [userId, '%' + searchText + '%', '%' + searchText + '%', sendIndex], function (err, result) {
                        if (err) {
                            callback(err);
                        } else {
                            sendData.result = result;
                            callback(null);
                        }
                    });
            }
        ], function (err) {
            if (err) {
                res.status(500).send();
            } else {
                if (sendData.result.length <= 18) {
                    sendData.end = true
                } else {
                    sendData.result = sendData.result.slice(0, 18);
                }
                callback();
            }
        });
    };

    findArtwork(function () {
        res.status(200).send(sendData);
    });
};

/*
 * browse - all artist의 정보를 ajax로 가져온다.
 */
exports.handleAllArtistQueryRequest = function (req, res) {
    var sendIndex = parseInt(req.body.sendIndex);

    var sendData = {
        end: false,
        result: undefined,
        flag: 'artist'
    };

    var findAllArtist = function (callback) {
        async.waterfall([
            function (callback) {
                mysqlConn.query('SELECT id FROM user WHERE email=?', [req.session.useremail], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, result[0].id);
                    }
                });
            }, function (userId, callback) {
                mysqlConn.query('SELECT userT.id id, userT.profile_url profile_url, userT.nickname nickname, ' +
                    'IF(subscribe_artist.id REGEXP \'^[0-9]+$\', 1, 0) sub_id ' +
                    'FROM user userT LEFT JOIN (SELECT user_subscribe_artist.artist_id id ' +
                    'FROM user user JOIN user_subscribe_artist ON user.id = user_subscribe_artist.user_id ' +
                    'WHERE user.id = ?) subscribe_artist ON userT.id = subscribe_artist.id ' +
                    'ORDER by userT.join_time DESC LIMIT ?,19',
                    [userId, sendIndex], function (err, result) {
                        if (err) {
                            callback(err);
                        } else {
                            sendData.result = result;
                            callback(null);
                        }
                    });
            }
        ], function (err) {
            if (err) {
                res.status(500).send();
            } else {
                if (sendData.result.length <= 18) {
                    sendData.end = true
                } else {
                    sendData.result = sendData.result.slice(0, 18);
                }
                callback();
            }
        });
    };

    findAllArtist(function () {
        res.status(200).send(sendData);
    });
};

/*
 * browse - artist의 정보를 ajax로 가져온다.
 */
exports.handleArtistQueryRequest = function (req, res) {
    var searchText = req.params.searchText;
    var sendIndex = parseInt(req.body.sendIndex);

    var sendData = {
        end: false,
        result: undefined,
        flag: 'artist'
    };

    var findArtist = function (callback) {
        async.waterfall([
            function (callback) {
                mysqlConn.query('SELECT id FROM user WHERE email=?', [req.session.useremail], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, result[0].id);
                    }
                });
            }, function (userId, callback) {
                mysqlConn.query('SELECT userT.id id, userT.profile_url profile_url, userT.nickname nickname, ' +
                    'IF(subscribe_artist.id REGEXP \'^[0-9]+$\', 1, 0) sub_id FROM user userT ' +
                    'LEFT JOIN (SELECT user_subscribe_artist.artist_id id FROM user user ' +
                    'JOIN user_subscribe_artist ON user.id = user_subscribe_artist.user_id ' +
                    'WHERE user.id = ?) subscribe_artist ON userT.id = subscribe_artist.id ' +
                    'WHERE userT.nickname LIKE ? OR userT.email LIKE ? ORDER by userT.join_time DESC LIMIT ?,19;',
                    [userId, '%' + searchText + '%', '%' + searchText + '%', sendIndex], function (err, result) {
                        if (err) {
                            callback(err);
                        } else {
                            sendData.result = result;
                            callback(null);
                        }
                    });
            }
        ], function (err) {
            if (err) {
                res.status(500).send();
            } else {
                if (sendData.result.length <= 18) {
                    sendData.end = true
                } else {
                    sendData.result = sendData.result.slice(0, 18);
                }
                callback();
            }
        });
    };

    findArtist(function () {
        res.status(200).send(sendData);
    });
};

exports.handleInstantArtworkQueryRequest = function (req, res) {
    var searchText = req.params.searchText;
    var sendData = {};

    async.parallel([
        function (callback) {
            mysqlConn.query('SELECT tag FROM hashtag ORDER BY search_count DESC LIMIT 3', function (err, result) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(err, result);
                }
            });
        }, function (callback) {
            mysqlConn.query('SELECT tag FROM hashtag WHERE tag LIKE ? ORDER BY search_count DESC LIMIT 1',
                [searchText + '%'], function (err, result) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(err, result);
                    }
                });
        }
    ], function (err, results) {
        if (err) {
            res.status(500).send();
        } else {
            if (results[1].length === 0) {
                sendData.result1 = results[0][0].tag;
                sendData.result2 = results[0][1].tag;
                sendData.result3 = results[0][2].tag;
            } else {
                if (results[0][0].tag === results[1][0].tag) {
                    sendData.result1 = results[1][0].tag;
                    sendData.result2 = results[0][1].tag;
                    sendData.result3 = results[0][2].tag;
                } else if (results[0][1].tag === results[1][0].tag) {
                    sendData.result1 = results[1][0].tag;
                    sendData.result2 = results[0][0].tag;
                    sendData.result3 = results[0][2].tag;
                } else if (results[0][2].tag === results[1][0].tag) {
                    sendData.result1 = results[1][0].tag;
                    sendData.result2 = results[0][0].tag;
                    sendData.result3 = results[0][1].tag;
                } else {
                    sendData.result1 = results[1][0].tag;
                    sendData.result2 = results[0][0].tag;
                    sendData.result3 = results[0][1].tag;
                }
            }
            res.status(200).send(sendData);
        }
    });
};

exports.handleInstantArtistQueryRequest = function (req, res) {
    var searchText = req.params.searchText;
    var sendData = {};

    async.parallel([
        function (callback) {
            mysqlConn.query('SELECT nickname FROM user ORDER BY search_count DESC LIMIT 3', function (err, result) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(err, result);
                }
            });
        }, function (callback) {
            mysqlConn.query('SELECT nickname FROM user WHERE nickname LIKE ? ORDER BY search_count DESC LIMIT 1',
                [searchText + '%'], function (err, result) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(err, result);
                    }
                });
        }
    ], function (err, results) {
        if (err) {
            res.status(500).send();
        } else {
            if (results[1].length === 0) {
                sendData.result1 = results[0][0].nickname;
                sendData.result2 = results[0][1].nickname;
                sendData.result3 = results[0][2].nickname;
            } else {
                if (results[0][0].nickname === results[1][0].nickname) {
                    sendData.result1 = results[1][0].nickname;
                    sendData.result2 = results[0][1].nickname;
                    sendData.result3 = results[0][2].nickname;
                } else if (results[0][1].nickname === results[1][0].nickname) {
                    sendData.result1 = results[1][0].nickname;
                    sendData.result2 = results[0][0].nickname;
                    sendData.result3 = results[0][2].nickname;
                } else if (results[0][2].nickname === results[1][0].nickname) {
                    sendData.result1 = results[1][0].nickname;
                    sendData.result2 = results[0][0].nickname;
                    sendData.result3 = results[0][1].nickname;
                } else {
                    sendData.result1 = results[1][0].nickname;
                    sendData.result2 = results[0][0].nickname;
                    sendData.result3 = results[0][1].nickname;
                }
            }
            res.status(200).send(sendData);
        }
    });
};