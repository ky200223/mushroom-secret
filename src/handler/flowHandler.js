/**
 * Created by youngkim on 2014. 11. 12..
 */

'use strict';

var mysqlConn = require(SOURCE_ROOT + '/module/mysql').getConnection();
var async = require('async');

/*
 * flow 페이지를 보여준다. (자신이 subscribe한 artist의 모든 작품을 최신순으로 보여준다. -> 18개까지)
 */
exports.viewFlowPage = function (req, res) {
    var renderData = {
        result: undefined,
        login: true,
        nickname: req.session.nickname,
        end: false
    };

    async.waterfall([
        function (callback) {
            mysqlConn.query('SELECT id FROM user WHERE email=?', [req.session.useremail], function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    callback(null, result[0].id);
                }
            })
        }, function (userId, callback) {
            mysqlConn.query('SELECT IF(like_rel.rel_flag = 1, 1, 0) like_flag, user.nickname nickname, ' +
                'art.introduction introduction, art.thumbnail_url thumbnail_url, art.id id, art.artist artistId, ' +
                'art.title title FROM artwork art JOIN ' +
                '(SELECT user.id id, user.nickname nickname FROM user user WHERE user.id in ' +
                '(SELECT artist_id FROM user_subscribe_artist WHERE user_id = ?)) subArtist ON subArtist.id = art.artist ' +
                'LEFT JOIN (SELECT rel.artwork_id rel_art, rel.like_flag rel_flag ' +
                'FROM user_relation_artwork rel WHERE rel.user_id = ?) like_rel ON art.id = like_rel.rel_art ' +
                'JOIN user user ON user.id = art.artist ORDER by art.upload_time DESC LIMIT 19'
                , [userId, userId], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        renderData.result = result;
                        callback(null);
                    }
                });
        }
    ], function (err) {
        if (err) {
            console.log(err);
            renderData.message = 'please try again';
            res.render('error', renderData);
        } else {
            if (renderData.result.length <= 18) {
                renderData.end = true
            } else {
                renderData.result = renderData.result.slice(0, 18);
            }
            res.render('flow', renderData);
        }
    });
};

/*
 * liked 페이지를 보여준다. (자신이 like한 모든 작품을 최신순으로 보여준다. -> 18개까지)
 */
exports.viewLikedPage = function (req, res) {
    var renderData = {
        result: undefined,
        login: true,
        nickname: req.session.nickname,
        end: false
    };

    async.waterfall([
        function (callback) {
            mysqlConn.query('SELECT id FROM user WHERE email=?', [req.session.useremail], function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    callback(null, result[0].id);
                }
            })
        }, function (userId, callback) {
            mysqlConn.query('SELECT user.nickname, art.thumbnail_url, art.id, art.introduction, art.artist artistId, ' +
            'art.title title FROM user JOIN (SELECT * FROM artwork WHERE artwork.id in ' +
            '(SELECT artwork_id FROM user_relation_artwork ' +
            'WHERE like_flag = 1 AND user_id = ?)) art ON art.artist = user.id ' +
            'ORDER by art.upload_time DESC LIMIT 19', [userId], function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    renderData.result = result;
                    callback(null);
                }
            });
        }
    ], function (err) {
        if (err) {
            renderData.message = 'find liked artwork flow failed. please try again';
            res.render('error', renderData);
        } else {
            if (renderData.result.length <= 18) {
                renderData.end = true
            } else {
                renderData.result = renderData.result.slice(0, 18);
            }
            res.render('liked', renderData);
        }
    });
};

/*
 * subscribe 페이지를 보여준다. (자신이 subscribe한 artist를 최신순으로 보여준다. -> 18개까지)
 */
exports.viewSubscribedPage = function (req, res) {
    var renderData = {
        result: undefined,
        login: true,
        nickname: req.session.nickname,
        end: false
    };

    async.waterfall([
        function (callback) {
            mysqlConn.query('SELECT id FROM user WHERE email=?', [req.session.useremail], function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    callback(null, result[0].id);
                }
            })
        }, function (userId, callback) {
            mysqlConn.query('SELECT * FROM user WHERE user.id in (SELECT artist_id FROM user_subscribe_artist ' +
            'WHERE user_id = ?) ORDER by user.join_time DESC LIMIT 19', [userId], function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    renderData.result = result;
                    callback(null);
                }
            });
        }
    ], function (err) {
        if (err) {
            renderData.message = 'find subscribed artist flow failed. please try again';
            res.render('error', renderData);
        } else {
            if (renderData.result.length <= 18) {
                renderData.end = true
            } else {
                renderData.result = renderData.result.slice(0, 18);
            }
            res.render('subscribed', renderData);
        }
    });
};

/*
 * flow 페이지 정보를 Ajax로 더 가져온다. -> 18개까지
 */
exports.handleFlowQueryRequest = function (req, res) {
    var sendIndex = parseInt(req.body.sendIndex);

    var sendData = {
        end: false,
        result: undefined,
        flag: 'artwork'
    };

    async.waterfall([
        function (callback) {
            mysqlConn.query('SELECT id FROM user WHERE email=?', [req.session.useremail], function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    callback(null, result[0].id);
                }
            })
        }, function (userId, callback) {
            mysqlConn.query('SELECT IF(like_rel.rel_flag = 1, 1, 0) like_flag, user.nickname nickname, ' +
                'art.introduction introduction, art.thumbnail_url thumbnail_url, art.id id, art.artist artistId,' +
                'art.title title FROM artwork art JOIN ' +
                '(SELECT user.id id, user.nickname nickname FROM user user WHERE user.id in ' +
                '(SELECT artist_id FROM user_subscribe_artist WHERE user_id = ?)) subArtist ON subArtist.id = art.artist ' +
                'LEFT JOIN (SELECT rel.artwork_id rel_art, rel.like_flag rel_flag ' +
                'FROM user_relation_artwork rel WHERE rel.user_id = ?) like_rel ON art.id = like_rel.rel_art ' +
                'JOIN user user ON user.id = art.artist ORDER by art.upload_time DESC LIMIT ?, 19'
                , [userId, userId, sendIndex], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        sendData.result = result;
                        callback(null);
                    }
                });
        }
    ], function (err) {
        if (err) {
            res.status(500).send();
        } else {
            if (sendData.result.length <= 18) {
                sendData.end = true
            } else {
                sendData.result = sendData.result.slice(0, 18);
            }
            res.status(200).send(sendData);
        }
    });
};

/*
 * liked 페이지 정보를 Ajax로 더 가져온다. -> 18개까지
 */
exports.handleLikedQueryRequest = function (req, res) {
    var sendIndex = parseInt(req.body.sendIndex);

    var sendData = {
        end: false,
        result: undefined,
        flag: 'artwork'
    };

    async.waterfall([
        function (callback) {
            mysqlConn.query('SELECT id FROM user WHERE email=?', [req.session.useremail], function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    callback(null, result[0].id);
                }
            })
        }, function (userId, callback) {
            mysqlConn.query('SELECT user.nickname, art.thumbnail_url, art.id, art.introduction, art.artist artistId, ' +
            'art.title title FROM user JOIN (SELECT * FROM artwork WHERE artwork.id in ' +
            '(SELECT artwork_id FROM user_relation_artwork ' +
            'WHERE like_flag = 1 AND user_id = ?)) art ON art.artist = user.id ' +
            'ORDER by art.upload_time DESC LIMIT ?, 19', [userId, sendIndex], function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    sendData.result = result;
                    callback(null);
                }
            });
        }
    ], function (err) {
        if (err) {
            res.status(500).send();
        } else {
            if (sendData.result.length <= 18) {
                sendData.end = true
            } else {
                sendData.result = sendData.result.slice(0, 18);
            }
            res.status(200).send(sendData);
        }
    });

};

/*
 * subscribe 페이지 정보를 Ajax로 더 가져온다. -> 18개까지
 */
exports.handleSubscribeQueryRequest = function (req, res) {
    var sendIndex = parseInt(req.body.sendIndex);

    var sendData = {
        end: false,
        result: undefined,
        flag: 'artist'
    };

    async.waterfall([
        function (callback) {
            mysqlConn.query('SELECT id FROM user WHERE email=?', [req.session.useremail], function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    callback(null, result[0].id);
                }
            })
        }, function (userId, callback) {
            mysqlConn.query('SELECT * FROM user WHERE user.id in (SELECT artist_id FROM user_subscribe_artist ' +
            'WHERE user_id = ?) ORDER by user.join_time DESC LIMIT ?,19', [userId, sendIndex], function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    sendData.result = result;
                    callback(null);
                }
            });
        }
    ], function (err) {
        if (err) {
            res.status(500).send();
        } else {
            if (sendData.result.length <= 18) {
                sendData.end = true
            } else {
                sendData.result = sendData.result.slice(0, 18);
            }
            res.status(200).send(sendData);
        }
    });

};