/**
 * Created by youngkim on 2014. 11. 12..
 */

//'use strict';

var mysqlConn = require(SOURCE_ROOT + '/module/mysql').getConnection();
var async = require('async');

/*
 * 유저가 로그아웃하게 한다. 쿠키 및 세션 정보를 비워준다.
 */
var setLogout = function (req, res) {
    req.session.isAdmin = false;
    req.session.loginStatus = false;
    req.session.useremail = undefined;
    req.session.nickname = undefined;

    res.clearCookie('googleplus', {path: '/user'});
    res.clearCookie('facebook', {path: '/user'});

    res.cookie('googleplus', 'false', { maxAge: 900000, httpOnly: true });
    res.cookie('facebook', 'false', { maxAge: 900000, httpOnly: true });
};

/*
 * 유저 정보를 조회하고 해당 정보로 유저 페이지를 렌더링.
 */
exports.viewUploadPage = function (req, res) {
    var renderData = {
        login: true,
        nickname: req.session.nickname
    };

    mysqlConn.query('SELECT * FROM user WHERE email =?', [req.session.useremail], function (err, result) {
        if (err) {
            renderData.message = 'Sorry, Try again please';
            res.render('error', renderData);
        } else {
            if (result.length === 0) {
                setLogout(req, res);
                res.render('error', {message: 'please login and retry', login: false})
            } else {
                renderData.userData = result[0];

                for (var index in renderData.userData) {
                    if (renderData.userData.hasOwnProperty(index)) {
                        var attr = renderData.userData[index];
                        if (attr === '' || attr === null) {
                            renderData.userData[index] = '';
                        }
                    }
                }

                if (renderData.userData.googleplus_login === 1) {
                    renderData.googleplus_link = true;
                    renderData.googleplus_address = renderData.userData.googleplus;
                } else {
                    renderData.googleplus_link = false;
                    renderData.googleplus_address = '';
                }
                if (renderData.userData.facebook_login === 1) {
                    renderData.facebook_link = true;
                    renderData.facebook_address = renderData.userData.facebook;
                } else {
                    renderData.facebook_link = false;
                    renderData.facebook_address = '';
                }

                res.render('upload', renderData);
            }
        }
    });
};

/*
 * 각 해시태그를 데이터베이스에 넣어준다. 각 태그는 유니크하기때문에 존재 여부를 확인한 후 넣어준다.
 */
var insertHashTag = function (artworkId, hashTag, callback) {
    if (hashTag !== '') {
        mysqlConn.query('SELECT id FROM hashtag WHERE tag = ?', [hashTag], function (err, result) {
            if (err) {
                throw err;
            }
            if (result.length === 0) {
                mysqlConn.query('INSERT INTO hashtag (tag) VALUES (?)', [hashTag], function (err) {
                    if (err) {
                        throw err;
                    }
                    mysqlConn.query('SELECT id FROM hashtag WHERE tag = ?', [hashTag], function (err, result) {
                        if (err) {
                            throw err;
                        }
                        var hashTagId = result[0].id;
                        mysqlConn.query('INSERT INTO artwork_has_hashtag (artwork_id, hashtag_id) VALUES (?, ?)', [artworkId, hashTagId], function (err) {
                            if (err) {
                                throw err;
                            }
                            callback();
                        });
                    });
                });
            } else {
                var hashTagId = result[0].id;
                mysqlConn.query('INSERT INTO artwork_has_hashtag (artwork_id, hashtag_id) VALUES (?, ?)', [artworkId, hashTagId], function (err) {
                    if (err) {
                        throw err;
                    }
                    callback();
                });
            }
        });
    } else {
        callback();
    }
};

/*
 * 업로드 정보를 데이터베이스에 넣어준다.
 */
exports.handleUploadArtworkRequest = function (req, res) {
    var renderData = {
        message: '',
        login: true,
        nickname: req.session.nickname
    };

    var time = new Date(new Date().toJSON());

    var user = {
        address: req.body.address || '',
        homepage: req.body.homepage || '',
        gallery: req.body.gallery || '',
        introduction: req.body.userIntroduction || ''
    };

    if (user.introduction != '') {
        user.introduction.replace(/\n/g, '<br>');
    }

    var artWork = {
        artist: '',
        photo_url: '',
        title: req.body.title || '',
        width: req.body.width || '',
        height: req.body.height || '',
        depth: req.body.depth || '',
        material: req.body.material || '',
        introduction: req.body.artworkIntroduction || '',
        upload_time: time
    };

    if (artWork.introduction != '') {
        artWork.introduction.replace(/\n/g, '<br>');
    }

    var hash = {
        hash1: req.body.hash1 || '',
        hash2: req.body.hash2 || '',
        hash3: req.body.hash3 || '',
        hash4: req.body.hash4 || '',
        hash5: req.body.hash5 || ''
    };

    //해시의 앞뒤 공백을 제거
    hash.hash1 = hash.hash1.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    hash.hash2 = hash.hash2.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    hash.hash3 = hash.hash3.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    hash.hash4 = hash.hash4.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    hash.hash5 = hash.hash5.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

    async.waterfall([
        function (callback) {
            mysqlConn.query('SELECT nickname FROM user WHERE nickname = ?', [req.body.nickname], function (err, result) {
                if (err) {
                    callback(err);
                }
                if (result.length === 0) {
                    mysqlConn.query('UPDATE user SET nickname = ? WHERE email =?', [req.body.nickname, req.session.useremail], function (err) {
                        if (err) {
                            callback(err);
                        }
                        renderData.nickname = req.body.nickname;
                        callback(null);
                    });
                } else {
                    callback(null);
                }
            });
        },
        function (callback) {
            if (req.body.profilePicture != undefined) {
                user.profile_url = req.body.profilePicture;
            }
            mysqlConn.query('UPDATE user SET ? WHERE email = ?', [user, req.session.useremail], function (err) {
                if (err) {
                    callback(err);
                } else {
                    callback(null);
                }
            });
        },
        function (callback) {
            if (req.body.artWorkPicture != undefined) {
                artWork.photo_url = req.body.artWorkPicture;
            }
            if (req.body.artWorkThumbnail != undefined) {
                artWork.thumbnail_url = req.body.artWorkThumbnail;
            } else {
                artWork.thumbnail_url = req.body.artWorkPicture;
            }

            mysqlConn.query('SELECT id FROM user WHERE email = ? ', [req.session.useremail], function (err, result) {
                if (err) {
                    callback(err);
                }
                artWork.artist = result[0].id;
                mysqlConn.query('INSERT INTO artwork SET ?', [artWork], function (err) {
                    if (err) {
                        callback(err);
                    }
                    callback(null);
                });
            });
        },
        function (callback) {
            var photo_url = artWork.photo_url;
            mysqlConn.query('SELECT id FROM artwork WHERE photo_url = ?', [photo_url], function (err, result) {
                if (err) {
                    callback(err);
                }
                var artwork_id = result[0].id;

                insertHashTag(artwork_id, hash.hash1, function () {
                    insertHashTag(artwork_id, hash.hash2, function () {
                        insertHashTag(artwork_id, hash.hash3, function () {
                            insertHashTag(artwork_id, hash.hash4, function () {
                                insertHashTag(artwork_id, hash.hash5, function () {
                                    callback(null);
                                });
                            });
                        });
                    });
                });
            });
        }
    ], function (err) {
        if (err) {
            error('error occurred : ' + err + '/// while handle artwork upload ' + req.session.useremail + '// profile and artwork');
            renderData.message = 'Error occurred while upload your artwork. sorry, please try again';
            res.render('error', renderData);
        } else {
            renderData.message = 'Successfully uploaded!';
            res.render('error', renderData);
        }
    });
};