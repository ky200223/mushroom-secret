/**
 * Created by youngkim on 2014. 11. 21..
 */
'use strict';

var mysqlConn = require(SOURCE_ROOT + '/module/mysql').getConnection();
var async = require('async');

/*
 * artwork의 정보를 ajax로 가져온다.
 */
exports.handleArtworkInfoRequest = function (req, res) {
    var artworkId = req.params.artwork_id;
    var userId = req.query.userId;

    var artworkInfo = {};

    async.waterfall([
        function (callback) {
            mysqlConn.query('SELECT * FROM artwork WHERE id =?', [artworkId], function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    if (result.length === 0) {
                        callback(new Error('invalid artwork id'));
                    } else {
                        artworkInfo.artwork = result[0];
                        callback(null);
                    }
                }
            });
        },
        function (callback) {
            if (artworkInfo.artwork.width === '') {
                artworkInfo.artwork.width = 'W';
            }
            if (artworkInfo.artwork.height === '') {
                artworkInfo.artwork.height = 'H';
            }
            if (artworkInfo.artwork.depth === '') {
                artworkInfo.artwork.depth = 'D';
            }
            mysqlConn.query('SELECT tag FROM artwork_has_hashtag JOIN hashtag ' +
                'WHERE artwork_has_hashtag.artwork_id = ? AND artwork_has_hashtag.hashtag_id = hashtag.id',
                [artworkInfo.artwork.id], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        if (result.length === 0) {
                            artworkInfo.artwork.hashtag = '';
                        } else {
                            artworkInfo.artwork.hashtag = result;
                        }
                        callback(null);
                    }
                });
        },
        function (callback) {
            mysqlConn.query('SELECT ac.id, ac.artwork_id, ac.user_id, ac.content, DATE_FORMAT(ac.comment_time, \'%Y%m%d\') as comment_time, u.nickname FROM artwork_comment ac ' +
                'JOIN user u WHERE ac.user_id = u.id AND ac.artwork_id = ?',
                [artworkInfo.artwork.id], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        if (result.length === 0) {
                            artworkInfo.artwork.comment = null;
                        } else {
                            artworkInfo.artwork.comment = result;
                        }
                        callback(null);
                    }
                });
        },
        function (callback) {
            mysqlConn.query('SELECT * FROM user_relation_artwork WHERE artwork_id =? AND user_id =?',
                [artworkId, userId], function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        if (result.length === 0 || result[0].like_flag === 0) {
                            artworkInfo.artwork.like = false;
                        } else {
                            artworkInfo.artwork.like = true;
                        }
                        callback(null);
                    }
                });
        }
    ], function (err) {
        if (err) {
            error('error occurred : ' + err + 'while load ' + req.session.useremail + ' artwork');
            res.status(200).send({status: 'fail'});
        } else {
            res.status(200).send(artworkInfo);
        }
    });
};

/*
 * comment를 add하는 기능
 */
exports.handleCommentAddRequest = function (req, res) {
    var commentData = {
        artwork_id: parseInt(req.body.artwork_id),
        content: req.body.commentContent,
        comment_time: new Date(new Date().toJSON())
    };

    var sendData = {
        status: 'success',
        commenterNickname: undefined,
        commentId: undefined,
        time: commentData.comment_time
    };

    async.waterfall([
        function (callback) {
            mysqlConn.query('SELECT id, nickname FROM user WHERE email =?', [req.session.useremail], function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    if (result.length === 0) {
                        callback(new Error(' invalid user id '));
                    } else {
                        commentData.user_id = result[0].id;
                        sendData.user_id = result[0].id;
                        sendData.commenterNickname = result[0].nickname;
                        callback(null);
                    }
                }
            });
        },
        function (callback) {
            mysqlConn.query('INSERT INTO artwork_comment SET ?', commentData, function (err, result) {
                if (err) {
                    callback(new Error(' fail to insert comment '));
                } else {
                    sendData.commentId = result.insertId;
                    callback(null);
                }
            })
        }
    ], function (err) {
        if (err) {
            error('error occurred : ' + err + ' //// while add comment ' + commentData.user_id
            + ' to ' + commentData.artwork_id + ' /// handle comment add request');
            sendData.status = 'fail';
            res.status(500).send(sendData);
        } else {
            res.status(200).send(sendData);
        }
    });
};

/*
 * comment를 delete한다.
 */
exports.handleCommentDeleteRequest = function (req, res) {
    var commentId = req.body.commentId;

    mysqlConn.query('DELETE FROM artwork_comment WHERE id = ?', [commentId], function (err) {
        if (err) {
            res.status(500).send({status: 'fail'});
        } else {
            res.status(200).send({status: 'success'});
        }
    });
};

/*
 * comment를 report한다.
 */
exports.handleCommentReportRequest = function (req, res) {
    var reportContent = req.body.reportContent;
    var commentId = req.body.commentId;

    mysqlConn.query('SELECT * FROM artwork_comment WHERE id = ?', [commentId], function (err, result) {
        if (err) {
            res.status(500).send({status: 'fail'});
        } else {
            sendReportMail(result[0], reportContent);
            res.status(200).send({status: 'success'});
        }
    });

};

/*
 * report 메일을 보낸다.
 */
var sendReportMail = function (reportComment, reportContent) {
    var mailModule = require(SOURCE_ROOT + '/module/mail');
    var mailConn = mailModule.createConn();

    var mailOptions = {
        from: "Mushroom <info@mushroom.land>", // sender address
        to: 'info@mushroom.land', // list of receivers
        subject: "reportContent", // Subject line
        html: "reportComment Info : " + reportComment.content + "<br/><br/>" + reportContent + "<br/>reported"
    };

    mailConn.sendMail(mailOptions, function (err) {
        if (err) {

        } else {
            info("Report Message sent to : " + 'ky200223@gmail.com')
        }
    });
};

/*
 * comment를 edit한다.
 */
exports.handleCommentEditRequest = function (req, res) {
    var commentId = req.body.commentId;
    var commentContent = req.body.content;

    mysqlConn.query('UPDATE artwork_comment SET content = ? WHERE id =?',
        [commentContent, commentId], function (err) {
            if (err) {
                res.status(500).send({status: 'fail'});
            } else {
                res.status(200).send({status: 'success'});
            }
        });
};