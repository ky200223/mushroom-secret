/**
 * Created by youngkim on 2014. 11. 5..
 */

'use strict';

var env = process.env.WRITE_SERVER_SETTING || 'development';

//set basic express module
var express = require('express'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    multer = require('multer'),
    RedisStore = require('connect-redis')(session);

var app = express();

app.use(cookieParser());

// view engine setup
app.set('views', PROJECT_ROOT + '/views');
app.set('view engine', 'jade');
app.use(express.static(PROJECT_ROOT + '/public'));

//set environment
app.set('env', env);

//set middleware
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(session({
    secret: '<mysecret>',
    saveUninitialized: true,
    resave: true,
    store: new RedisStore()
}));
app.use(multer());
app.use(function (req, res, next) {
    if (req.session === undefined || req.session.loginStatus === undefined) {
        req.session.loginStatus = false;
    }
    next();
});
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "*");
    next();
});

//set router
var mainRouter = require(SOURCE_ROOT + '/router/mainRouter');

mainRouter(app);

// error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development' || app.get('env') === 'local') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    error(err);
//    res.render('error', {
//        message: err.message,
//        error: {}
//    });
});

module.exports = app;
